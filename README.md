# LiViTo

A prototypical software tool to assess linguistic and visual features of handwritten texts.

**Funding**:

This work was supported by VolkswagenStiftung [A118750, Mixed Methods Program].
