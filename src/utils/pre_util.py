import os
import cv2
import numpy as np
import pickle
import pathlib
from skimage import measure
from tqdm import tqdm_notebook
from datetime import datetime
import warnings
import utils.preprocess.extract_text_lines as etl
warnings.filterwarnings("ignore")


def line_coordinate_extraction(sep_seams, img_bin, output_path):
    img = img_bin
    height = img.shape[0]
    width = sep_seams.shape[0]
    # empty white array of shape of input image
    first_row = np.zeros((width, 1), dtype=np.float64)
    last_row = np.ones((width, 1), dtype=np.float64) * (img.shape[0] - 10)
    new_stack = np.hstack((first_row, sep_seams, last_row))
    width, num_of_lines = new_stack.shape

    coords = list()
    # save left and right coordinates of detected line
    for i in range(num_of_lines - 1):
        img_dummy = np.ones((height, width), dtype=np.uint8) * 255
        for j in range(width):
            try:
                img_dummy[int(new_stack[j, i]):int(new_stack[j, i + 1]), j] = img[int(new_stack[j, i]):int(
                    new_stack[j, i + 1]), j]
            except:
                continue
        # background has value 255
        # set first and last 50 rows to background value
        gap = 50
        img_dummy[:, 0:gap] = 255
        img_dummy[:, -gap:] = 255
        # blur image vertically
        kernel = np.ones((10, 115), dtype=np.uint8)
        kernel_erode = np.ones((5, 5), dtype=np.uint8)
        img_dummy[img_dummy == 0] = 1
        img_dummy[img_dummy == 255] = 0
        erode = cv2.erode(img_dummy, kernel_erode)
        dilation = cv2.dilate(erode, kernel=kernel, iterations=1)

        # get properties -> get area bounding box coordinates
        labels = measure.label(dilation, connectivity=2, background=0)
        region_properties = measure.regionprops(labels, coordinates="rc")
        # get largest region
        largest = [reg.area for reg in region_properties]
        if len(largest) == 0:
            coords.append(None)
        else:  # regular case
            largest_bbox = region_properties[largest.index(max(largest))].bbox
            coords.append((largest_bbox[1] + gap, largest_bbox[3] - gap))  # left min right max

    outfile = open(output_path.joinpath('line_coords.p'), 'wb')
    pickle.dump(coords, outfile)


def binarize_img(grey_image):
    # create threshold image 
    # you can play with the values 111 and 21 for better results
    th = cv2.adaptiveThreshold(grey_image, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 111, 21)
    # denoise image
    th[th == 0] = 1
    th[th == 255] = 0
    kernel = np.ones((2,2), np.uint8)
    bin_im = cv2.morphologyEx(th, cv2.MORPH_OPEN, kernel)
    bin_im[bin_im == 0] = 255
    bin_im[bin_im == 1] = 0
    return bin_im


def seperate_lines(image, seams):
    img_height = image.shape[0]
    img_width, seam_num = seams.shape
    seams = seams.astype(int)
    
    # first seam
    first_seam = seams[:, 0]
    max_height = int(np.max(first_seam))
    line = np.ones((max_height, img_width))*255
    for idx, border in enumerate(first_seam):
        line[0:border, idx] = image[0:border, idx]
    # save line
    if line.shape[0] > 300:
        # error message
        logfile.write(str(datetime.now())[:-7] + '\n')
        logfile.write('Cropping error in '+filename+' in line: ' + str(1) + '\n')
    else:
        cv2.imwrite(str(path.joinpath('output', filename, 'lines', filename+'_01.'+filetype)), line)
    
    # last seam
    last_seam = seams[:, seam_num-1]
    min_height = int(np.min(last_seam))
    line = np.ones((img_height-min_height, img_width))*255
    line_height = line.shape[0]
    for idx, border in enumerate(last_seam):
        line[line_height - (img_height - border):, idx] = image[border:, idx]
    # save line
    if seam_num < 9:
        if line.shape[0] > 300:
            # error message
            logfile.write(str(datetime.now())[:-7] + '\n')
            logfile.write('Cropping error in '+filename+' in line: ' + str(seam_num+1) + '\n')
        else:
            cv2.imwrite(str(path.joinpath('output', filename, 'lines', filename+'_0'+str(seam_num+1)+'.'+filetype)), line)
    else:
        if line.shape[0] > 300:
            # error message
            logfile.write(str(datetime.now())[:-7] + '\n')
            logfile.write('Cropping error in '+filename+' in line: ' + str(seam_num+1) + '\n')
        else:
            cv2.imwrite(str(path.joinpath('output', filename, 'lines', filename+'_'+str(seam_num+1)+'.'+filetype)), line)
        
    # remaining lines
    for idx in range(seam_num-1):
        seam = seams[:, idx]
        next_seam = seams[:, idx+1]
        line_min = int(np.min(seam))
        line_max = int(np.max(next_seam))
        line_height = line_max - line_min
        line = np.ones((line_height, img_width))*255
        for index in range(img_width):
            current_val = seam[index]
            next_val = next_seam[index]
            try:
                line[current_val-line_min:current_val-line_min + (next_val - current_val), index] = image[current_val:next_val, index]
            except ValueError:
                logfile.write(str(datetime.now())[:-7]+'\n')
                logfile.write('Error in line: '+str(idx)+'\n')
                print('Error in line: ', idx)
    # save cropped line
        if idx <= 7:
            if line.shape[0] > 300:
                # error message
                logfile.write(str(datetime.now())[:-7] + '\n')
                logfile.write('Cropping error in '+filename+' in line: ' + str(idx+2) + '\n')
            else:
                cv2.imwrite(str(path.joinpath('output', filename, 'lines', filename+'_0'+str(idx+2)+'.'+filetype)), line)
        else:
            if line.shape[0] > 300:
                # error message
                logfile.write(str(datetime.now())[:-7] + '\n')
                logfile.write('Cropping error in '+filename+' in line: ' + str(idx+2) + '\n')
            else:
                cv2.imwrite(str(path.joinpath('output', filename, 'lines', filename+'_'+str(idx+2)+'.'+filetype)), line)


def run_preprocessing(s, peak_dist):
    for filepath in tqdm_notebook(file_path_generator, total=total_files):
        # path preperation
        global filename
        filename = filepath.stem
        suffix = filepath.suffix
        path_out = path.joinpath('output', filename)
        line_dir = path.joinpath('output', filename, 'lines')

        if os.path.isdir(str(path_out)):
            continue

        # load files
        img = cv2.imread(str(filepath))
        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img_bin = binarize_img(img_gray)

        # apply seam detection
        try:
            medial_seams, sep_seams = etl.extract_text_lines(img, img_gray, img_bin, smooth, s, sigma, off, peak_dist)
        except IndexError:  # ValueError also occured
            logfile.write(str(datetime.now())[:-7]+'\n')
            logfile.write(str(path_out)+' will be skipped, as it is most likely an empty page. If page is not empty reduce variable s.'+'\n')
            continue

        # create directories
        try:
            os.mkdir(str(path_out))
        except FileExistsError:
            logfile.write(str(datetime.now())[:-7]+'\n')
            logfile.write(str(path_out) + ' already exists. Processing will be skipped.'+'\n')
            continue
        try:
            os.mkdir(str(line_dir))
        except FileExistsError:
            logfile.write(str(datetime.now())[:-7]+'\n')
            logfile.write(str(line_dir) + ' already exists. Processing will be skipped.'+'\n')
            continue

        # save original image
        cv2.imwrite(str(path_out.joinpath(filename+suffix)), img)
        # save separating seams and mean seams in a pickle file
        pickle.dump(medial_seams, open(path_out.joinpath('medial_seams.p'), 'wb'))
        pickle.dump(sep_seams, open(path_out.joinpath('separating_seams.p'), 'wb'))

        # crop and save lines
        seperate_lines(img_bin, sep_seams)

        # get line coordinates
        line_coordinate_extraction(sep_seams, img_bin, path_out)
    logfile.close()


smooth = 190000     # smoothing for projection profile matching
sigma = 3           # standard deviation for gaussian smoothing
off = 1             # offset for seam overlay, corresponds to seam thickness
filetype = 'tif'    # set file type

path = pathlib.Path().cwd().parent / 'data' / 'preprocess_data'
total_files = len(list(path.joinpath('input').glob('*.'+filetype)))
file_path_generator = path.joinpath('input').glob('*.'+filetype)
filename = ''
# start logging
logfile = open(path / 'log.txt', 'a')
