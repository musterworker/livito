import sys
import tensorflow as tf


class DecoderType:
	BestPath = 0


def batch_norm(input):
	epsilon = 1e-3
	tf.variable_scope(tf.get_variable_scope, reuse=True)
	scale = tf.get_variable(input.name[:-2] + '_bn_scale', [input.get_shape()[-1]], initializer=tf.constant_initializer(1))
	beta = tf.get_variable(input.name[:-2] + '_bn_beta', [input.get_shape()[-1]], initializer=tf.constant_initializer(0))
	batch_mean, batch_var = tf.nn.moments(input, [0, 1, 2])

	return tf.nn.batch_normalization(input, batch_mean, batch_var, beta, scale, epsilon)


class Model: 
	# minimalistic TF model for HTR

	# model constants
	batchSize = 12  # 50
	imgSize = (800, 64)
	maxTextLen = 100  # 128#32

	def __init__(self, charList, decoderType=DecoderType.BestPath, mustRestore=False):
		# init model: add CNN, RNN and CTC and initialize TF
		self.charList = charList
		self.decoderType = decoderType
		self.mustRestore = mustRestore
		self.snapID = 0

		# CNN
		self.inputImgs = tf.placeholder(tf.float32, shape=(Model.batchSize, Model.imgSize[0], Model.imgSize[1]))
		cnnOut4d = self.setupCNN(self.inputImgs)

		# RNN
		rnnOut3d = self.setupRNN(cnnOut4d)

		# CTC
		(self.loss, self.decoder) = self.setupCTC(rnnOut3d)

		# optimizer for NN parameters
		self.batchesTrained = 0
		self.learningRate = tf.placeholder(tf.float32, shape=[])
		self.optimizer = tf.train.RMSPropOptimizer(self.learningRate).minimize(self.loss)

		# initialize TF
		(self.sess, self.saver) = self.setupTF()

	def setupCNN(self, cnnIn3d):
		# create CNN layers and return output of these layers
		cnnIn4d = tf.expand_dims(input=cnnIn3d, axis=3)

		# create layers
		input = cnnIn4d  # input to first CNN layer

		kernel = tf.Variable(tf.truncated_normal([5, 5, 1, 64], stddev=0.1))
		conv_1 = tf.nn.conv2d(input=input, filter=kernel, padding='SAME', strides=(1, 1, 1, 1))
		relu_1 = tf.nn.selu(conv_1)
		pool_1 = tf.nn.max_pool(relu_1, ksize=(1, 2, 2, 1), strides=(1, 2, 2, 1), padding='VALID')

		kernel = tf.Variable(tf.truncated_normal([5, 5, 64, 128], stddev=0.1))
		conv_2 = tf.nn.conv2d(input=pool_1, filter=kernel, padding='SAME', strides=(1, 1, 1, 1))
		relu_2 = tf.nn.selu(conv_2)
		pool_2 = tf.nn.max_pool(relu_2, ksize=(1, 1, 2, 1), strides=(1, 1, 2, 1), padding='VALID')

		kernel = tf.Variable(tf.truncated_normal([3, 3, 128, 128], stddev=0.1))
		conv_3 = tf.nn.conv2d(input=pool_2, filter=kernel, padding='SAME', strides=(1, 1, 1, 1))
		relu_3 = tf.nn.selu(conv_3)
		pool_3 = tf.nn.max_pool(relu_3, ksize=(1, 2, 2, 1), strides=(1, 2, 2, 1), padding='VALID')
		bn_1 = batch_norm(pool_3)

		kernel = tf.Variable(tf.truncated_normal([3, 3, 128, 256], stddev=0.1))
		conv_4 = tf.nn.conv2d(input=bn_1, filter=kernel, padding='SAME', strides=(1, 1, 1, 1))
		relu_4 = tf.nn.selu(conv_4)

		kernel = tf.Variable(tf.truncated_normal([3, 3, 256, 256], stddev=0.1))
		conv_5 = tf.nn.conv2d(input=relu_4, filter=kernel, padding='SAME', strides=(1, 1, 1, 1))
		relu_5 = tf.nn.selu(conv_5)
		pool_4 = tf.nn.max_pool(relu_5, ksize=(1, 2, 2, 1), strides=(1, 2, 2, 1), padding='VALID')

		kernel = tf.Variable(tf.truncated_normal([3, 3, 256, 512], stddev=0.1))
		conv_6 = tf.nn.conv2d(input=pool_4, filter=kernel, padding='SAME', strides=(1, 1, 1, 1))
		relu_6 = tf.nn.selu(conv_6)
		pool_5 = tf.nn.max_pool(relu_6, ksize=(1, 1, 2, 1), strides=(1, 1, 2, 1), padding='VALID')
		bn_2 = batch_norm(pool_5)

		kernel = tf.Variable(tf.truncated_normal([3, 3, 512, 512], stddev=0.1))
		conv_7 = tf.nn.conv2d(input=bn_2, filter=kernel, padding='SAME', strides=(1, 1, 1, 1))
		relu_7 = tf.nn.selu(conv_7)
		pool_7 = tf.nn.max_pool(relu_7, ksize=(1, 1, 2, 1), strides=(1, 1, 2, 1), padding='VALID')

		return pool_7

	def setupRNN(self, rnnIn4d):
		# create RNN layers and return output of these layers
		rnnIn3d = tf.squeeze(rnnIn4d, axis=[2])

		# basic cells which is used to build RNN
		numHidden = 256
		cells = [tf.contrib.rnn.LSTMCell(num_units=numHidden, state_is_tuple=True) for _ in range(2)]  # 2 layers

		# stack basic cells
		stacked = tf.contrib.rnn.MultiRNNCell(cells, state_is_tuple=True)

		# bidirectional RNN
		# BxTxF -> BxTx2H
		((fw, bw), _) = tf.nn.bidirectional_dynamic_rnn(cell_fw=stacked, cell_bw=stacked, inputs=rnnIn3d, dtype=rnnIn3d.dtype)
									
		# BxTxH + BxTxH -> BxTx2H -> BxTx1x2H
		concat = tf.expand_dims(tf.concat([fw, bw], 2), 2)
									
		# project output to chars (including blank): BxTx1x2H -> BxTx1xC -> BxTxC
		kernel = tf.Variable(tf.truncated_normal([1, 1, numHidden * 2, len(self.charList) + 1], stddev=0.1))
		return tf.squeeze(tf.nn.atrous_conv2d(value=concat, filters=kernel, rate=1, padding='SAME'), axis=[2])

	def setupCTC(self, ctcIn3d):
		# create CTC loss and decoder and return them
		# BxTxC -> TxBxC
		self.ctcIn3dTBC = tf.transpose(ctcIn3d, [1, 0, 2])
		# ground truth text as sparse tensor
		self.gtTexts = tf.SparseTensor(tf.placeholder(tf.int64, shape=[None, 2]), tf.placeholder(tf.int32, [None]), tf.placeholder(tf.int64, [2]))
		# calc loss for batch
		self.seqLen = tf.placeholder(tf.int32, [None])
		loss = tf.nn.ctc_loss(labels=self.gtTexts, inputs=self.ctcIn3dTBC, sequence_length=self.seqLen, ctc_merge_repeated=True, ignore_longer_outputs_than_inputs=True)
		decoder = tf.nn.ctc_greedy_decoder(inputs=self.ctcIn3dTBC, sequence_length=self.seqLen)
		# return a CTC operation to compute the loss and a CTC operation to decode the RNN output
		return (tf.reduce_mean(loss), decoder)

	def setupTF(self):
		# initialize TF
		print('Python: '+sys.version)
		print('Tensorflow: '+tf.__version__)

		sess = tf.Session()
		# saver saves model to file
		saver = tf.train.Saver(max_to_keep=1)
		modelDir = 'utils/kws_util_training/model/'
		latestSnapshot = tf.train.latest_checkpoint(modelDir) # is there a saved model?

		# if model must be restored (for inference), there must be a snapshot
		if self.mustRestore and not latestSnapshot:
			raise Exception('No saved model found in: ' + modelDir)

		# load saved model if available
		if latestSnapshot and self.mustRestore:
			print('Init with stored values from ' + latestSnapshot)
			saver.restore(sess, latestSnapshot)
		else:
			print('Init with new values')
			sess.run(tf.global_variables_initializer())

		return (sess,saver)

	def toSparse(self, texts):
		# put ground truth texts into sparse tensor for ctc_loss
		indices = []
		values = []
		shape = [len(texts), 0]  # last entry must be max(labelList[i])

		# go over all texts
		for (batchElement, text) in enumerate(texts):
			# convert to string of label (i.e. class-ids)
			labelStr = [self.charList.index(c) for c in text]
			# sparse tensor must have size of max. label-string
			if len(labelStr) > shape[1]:
				shape[1] = len(labelStr)
			# put each label into sparse tensor
			for (i, label) in enumerate(labelStr):
				indices.append([batchElement, i])
				values.append(label)

		return (indices, values, shape)

	def decoderOutputToText(self, ctcOutput):
		# extract texts from output of CTC decoder
		
		# contains string of labels for each batch element
		encodedLabelStrs = [[] for i in range(Model.batchSize)]

		# TF decoders: label strings are contained in sparse tensor
		# ctc returns tuple, first element is SparseTensor 
		decoded = ctcOutput[0][0]

		# go over all indices and save mapping: batch -> values
		idxDict = { b : [] for b in range(Model.batchSize) }
		for (idx, idx2d) in enumerate(decoded.indices):
			label = decoded.values[idx]
			batchElement = idx2d[0] # index according to [b,t]
			encodedLabelStrs[batchElement].append(label)

		# map labels to chars for all batch elements
		return [str().join([self.charList[c] for c in labelStr]) for labelStr in encodedLabelStrs]

	def trainBatch(self, batch):
		# feed a batch into the NN to train it
		numBatchElements = len(batch.imgs)
		sparse = self.toSparse(batch.gtTexts)
		rate = 0.01 if self.batchesTrained < 10 else (0.001 if self.batchesTrained < 10000 else 0.0001) # decay learning rate
		evalList = [self.optimizer, self.loss]
		feedDict = {self.inputImgs: batch.imgs, self.gtTexts: sparse, self.seqLen: [Model.maxTextLen]*numBatchElements, self.learningRate: rate}
		(_, lossVal) = self.sess.run(evalList, feedDict)
		self.batchesTrained += 1
		return lossVal

	def inferBatch(self, batch):
		# feed a batch into the NN to recognize the texts
		numBatchElements = len(batch.imgs)
		feedDict = {self.inputImgs: batch.imgs, self.seqLen: [Model.maxTextLen] * numBatchElements}
		evalRes = self.sess.run([self.decoder, self.ctcIn3dTBC], feedDict)
		decoded = evalRes[0]
		return self.decoderOutputToText(decoded)

	def save(self):
		# save model to file
		self.snapID += 1
		self.saver.save(self.sess, 'utils/kws_util_training/model/snapshot', global_step=self.snapID)
