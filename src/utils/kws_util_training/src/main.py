# For better results you might need
# try not to use special characters like №, use rather No. Merge similar looking signs like '´` to one version.
# you might need to adjust -> DataLoader:self.numTrainSamplesPerEpoch = 600
# you might need to adjust -> Model:batchSize = 20
import argparse
import cv2
import os
import numpy as np
import glob
from tqdm import tqdm_notebook
import editdistance
from utils.kws_util_training.src.DataLoader import DataLoader, Batch
from utils.kws_util_training.src.Model import Model, DecoderType
from utils.kws_util_training.src.SamplePreprocessor import preprocess
from utils.kws_util_training.src.Model_trans import Model_trans
from utils.kws_util_training.src.ctc_reconstruction import run_ctc_reco
from IPython.display import clear_output

class FilePaths:
	# filenames and paths to data
	fnCharList = 'utils/kws_util_training/model/charList.txt'
	fnAccuracy = 'utils/kws_util_training/model/accuracy.txt'
	fnTrain = '../data/kws_data'
	fnInfer = '../data/test.jpg'
	fnCorpus = 'utils/kws_util_training/model/corpus.txt'


def train(model, loader):
	# train NN
	epoch = 0  # number of training epochs since start
	bestCharErrorRate = float('inf')  # best valdiation character error rate
	noImprovementSince = 0  # number of epochs no improvement of character error rate occurred
	earlyStopping = 3  # stop training after this number of epochs without improvement
	while True:
		epoch += 1
		print('Epoch:', epoch)

		# train
		print('Train NN')
		loader.trainSet()
		while loader.hasNext():
			iterInfo = loader.getIteratorInfo()
			batch = loader.getNext()
			# with tf.device('/device:GPU:1'):
			loss = model.trainBatch(batch)
			print('Batch:', iterInfo[0], '/', iterInfo[1], 'Loss:', loss)

		# validate
		charErrorRate = validate(model, loader)
		
		# if best validation accuracy so far, save model parameters
		if charErrorRate < bestCharErrorRate:
			print('Character error rate improved, save model')
			bestCharErrorRate = charErrorRate
			noImprovementSince = 0
			model.save()
			open(FilePaths.fnAccuracy, 'w').write('Validation character error rate of saved model: %f%%' % (charErrorRate*100.0))
		else:
			print('Character error rate not improved')
			noImprovementSince += 1

		# stop training if no more improvement in the last x epochs
		if noImprovementSince >= earlyStopping:
			print('No more improvement since %d epochs. Training stopped.' % earlyStopping)
			break


def validate(model, loader):
	# validate NN
	print('Validate NN')
	loader.validationSet()
	print(loader.samples[0].filePath)
	numCharErr = 0
	numCharTotal = 0
	numWordOK = 0
	numWordTotal = 0
	while loader.hasNext():
		iterInfo = loader.getIteratorInfo()
		print('Batch:', iterInfo[0], '/', iterInfo[1])
		batch = loader.getNext()
		recognized = model.inferBatch(batch)
		
		print('Ground truth -> Recognized')	
		for i in range(len(recognized)):
			numWordOK += 1 if batch.gtTexts[i] == recognized[i] else 0
			numWordTotal += 1
			dist = editdistance.eval(recognized[i], batch.gtTexts[i])
			numCharErr += dist
			numCharTotal += len(batch.gtTexts[i])
			print('[OK]' if dist == 0 else '[ERR:%d]' % dist, '"' + batch.gtTexts[i] + '"', '->', '"' + recognized[i] + '"')
	
	# print validation result
	try:
		charErrorRate = numCharErr / numCharTotal
	except:
		charErrorRate = 1
	try:
		wordAccuracy = numWordOK / numWordTotal
	except:
		wordAccuracy = 0
	print('Character error rate: %f%%. Word accuracy: %f%%.' % (charErrorRate*100.0, wordAccuracy*100.0))
	return charErrorRate


def infer(model, fnImg):
	# recognize text in image provided by file path"
	img = preprocess(cv2.imread(fnImg, cv2.IMREAD_GRAYSCALE), Model.imgSize)
	batch = Batch(None, [img]) # fill all batch elements with same input image...[img] * Model.batchSize
	# with tf.device('/device:GPU:1'):
	recognized = model.inferBatch(batch) # recognize text
	print('Recognized:', '"' + recognized[0] + '"')  # all batch elements hold same result


def infer_trans(model, fnImg, save_path):
	img = preprocess(cv2.imread(fnImg, cv2.IMREAD_GRAYSCALE), Model.imgSize)
	batch = Batch(None, [img] * Model.batchSize)  # fill all batch elements with same input image
	# with tf.device('/device:GPU:1'):
	(recognized, probability) = model.inferBatch_trans(batch, calcProbability=True)  # recognize text
	split = fnImg.split('\\')
	np.save(save_path+'/'+split[-1][:-4], probability)


def training():
	# optional command line args
	parser = argparse.ArgumentParser()
	parser.add_argument("--train", default=True, help="train the NN", action="store_true")
	parser.add_argument("--validate", default=True, help="validate the NN", action="store_true")
	args = parser.parse_args(args=[])

	decoderType = DecoderType.BestPath
	# train or validate on dataset	
	if args.train or args.validate:
		# load training data, create TF model
		loader = DataLoader(FilePaths.fnTrain, Model.batchSize, Model.imgSize, Model.maxTextLen)
		# save characters of model for inference mode
		open(FilePaths.fnCharList, 'w', encoding='utf-8-sig').write(str().join(loader.charList))
		# save words contained in dataset into file
		open(FilePaths.fnCorpus, 'w', encoding='utf-8-sig').write(str(' ').join(loader.trainWords + loader.validationWords))
		# execute training or validation
		if args.train:
			model = Model(loader.charList, decoderType, mustRestore=False)
			train(model, loader)
		elif args.validate:
			model = Model(loader.charList, decoderType, mustRestore=True)
			validate(model, loader)


def transcription():
	decoderType = DecoderType.BestPath
	charList = open(FilePaths.fnCharList, encoding='utf-8').read()
	model = Model_trans(charList, decoderType, mustRestore=True)
	# iterate over documents
	clear_output()
	print('Transcribing manuscripts:')
	for line in tqdm_notebook(sorted(glob.glob('../data/preprocess_data/output/*/lines/*.tif'))):
		splits = line.split('\\')
		save_path = '/'.join(splits[0:2]) + '/transcribtions'
		try:
			os.mkdir(save_path)
		except:
			pass
		infer_trans(model, line, save_path)
	# reconstruct output from transcription
	print('Reconstructing the output from the neural network:')
	run_ctc_reco()


