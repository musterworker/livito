import numpy as np
import math
import collections
import glob
import pickle
import os
from tqdm import tqdm_notebook

NEG_INF = -float("inf")


def make_new_beam():
    fn = lambda: (NEG_INF, NEG_INF)
    return collections.defaultdict(fn)


def make_new_prob_beam():
    fn = lambda: []
    return collections.defaultdict(fn)


def logsumexp(*args):
    # Stable log sum exp.
    if all(a == NEG_INF for a in args):
        return NEG_INF
    a_max = max(args)
    lsp = math.log(sum(math.exp(a - a_max) for a in args))
    return a_max + lsp


def decode(probs, beam_size=10, blank=0):
    # Performs inference for the given output probabilities.
    # Arguments:
    #     probs: The output probabilities (e.g. post-softmax) for each
    #       time step. Should be an array of shape (time x output dim).
    #     beam_size (int): Size of the beam to use during inference.
    #     blank (int): Index of the CTC blank label.
    # Returns the output label sequence and the corresponding negative
    # log-likelihood estimated by the decoder.

    T, S = probs.shape
    blank = S-1
    # Elements in the beam are (prefix, (p_blank, p_no_blank))
    # Initialize the beam with the empty sequence, a probability of
    # 1 for ending in blank and zero for ending in non-blank
    beams = list()
    beam = [(tuple(), (0.0, NEG_INF))]
    beam_probs = [[tuple(), [0]]]
    for t in range(T):  # Loop over time
        # A default dictionary to store the next step candidates.
        next_beam = make_new_beam()
        beam_probs = make_new_prob_beam()
        for s in range(S):  # Loop over vocab
            p = probs[t, s]
            if p > 0:
                # The variables p_b and p_nb are respectively the
                # probabilities for the prefix given that it ends in a
                # blank and does not end in a blank at this time step.
                for prefix, (p_b, p_nb) in beam:  # Loop over beam
                    # If we propose a blank the prefix doesn't change.
                    # Only the probability of ending in blank gets updated.
                    if s == blank:
                        # log-likelihood
                        n_p_b, n_p_nb = next_beam[prefix]
                        n_p_b = logsumexp(n_p_b, p_b + p, p_nb + p)
                        next_beam[prefix] = (n_p_b, n_p_nb)
                        continue
                    # Extend the prefix by the new character s and add it to
                    # the beam. Only the probability of not ending in blank
                    # gets updated.
                    end_t = prefix[-1] if prefix else None
                    n_prefix = prefix + (s,)
                    n_p_b, n_p_nb = next_beam[n_prefix]
                    # for scaled-likelhood change s -> charList[s]
                    if s != end_t:
                        # log-likelihood
                        n_p_nb = logsumexp(n_p_nb, p_b + p, p_nb + p)
                        # scaled-likelihood
                    else:
                        # We don't include the previous probability of not ending
                        # in blank (p_nb) if s is repeated at the end. The CTC
                        # algorithm merges characters not separated by a blank.
                        # log-likelihood
                        n_p_nb = logsumexp(n_p_nb, p_b + p)
                    # *NB* this would be a good place to include an LM score.
                    next_beam[n_prefix] = (n_p_b, n_p_nb)
                    # If s is repeated at the end we also update the unchanged
                    # prefix. This is the merging case.
                    if s == end_t:
                        # log-likelihood
                        n_p_b, n_p_nb = next_beam[prefix]
                        n_p_nb = logsumexp(n_p_nb, p_nb + p)
                        next_beam[prefix] = (n_p_b, n_p_nb)
        # Sort and trim the beam before moving on to the
        # next time-step.
        beam = sorted(next_beam.items(),
                      key=lambda x: logsumexp(*x[1]),
                      reverse=True)
        beam = beam[:20]
        beams.append(beam)
        # beam_probs crop like beams and sort
    return beams[-1]


def run_ctc_reco():
    # load char list
    chars = open('utils/kws_util_training/model/charList.txt', 'r', encoding='utf-8').readlines()
    charList = sorted(list(chars[0]))
    # random special character necessary for processing
    charList.append('|')
    # load rnn output
    for line_prob in tqdm_notebook(sorted(glob.glob('../data/preprocess_data/output/*/transcribtions/*.npy'))):
        splits = line_prob.split('\\')
        save_path = '/'.join(splits[0:2]) + '/trans_dicts'
        try:
            os.mkdir(save_path)
        except:
            pass
        probs = np.load(line_prob)
        # reduce rnn to n elements per time step and normalize
        time_step_sums = np.sum(probs, axis=0)
        rnn_probs = probs / time_step_sums
        prob_col_sum = np.sum(rnn_probs, axis=0)
        # choose n largest values from columns, set the rest to 0
        n = 5
        reduced_prob_set = np.zeros(shape=rnn_probs.shape)
        max_index = np.argsort(-rnn_probs, axis=0)[:n]
        for i in range(rnn_probs.shape[1]):
            for j in range(n):
                reduced_prob_set[max_index[j, i], i] = rnn_probs[max_index[j, i], i]
				
        prob_col_sum = np.sum(reduced_prob_set, axis=0)
        reduced_prob_set /= prob_col_sum
        probs_trans = np.transpose(reduced_prob_set)
        # labels, score = decode(probs_trans, beam_size=n)
        transcripts_index = decode(probs)

        #####################################post-processing#############################################
        # convert index transcription into char transcription
        if len(transcripts_index) == 0:
            # save dictionaries
            name = line_prob.split('\\')
            outfile = open(save_path + '/' + name[-1][:-4] + '.p', 'wb')
            pickle.dump([], outfile)
            outfile.close()
            print(name)
        else:
            transcripts = list()
            for trans in transcripts_index:
                decifer = list()
                for i in trans[0]:
                    decifer.append(charList[i])
                transcripts.append(decifer)

            # clean transcriptions, minimal operations
            transcripts_cleaned = list()
            for sample in transcripts:
                x = ''.join(sample)
                # merge consecutive signs and whitespaces
                x = x.replace('..', '.')
                x = x.replace('.', ' . ')
                x = x.replace(',,', ',')
                x = x.replace(',', ' , ')
                x = x.replace('::', ':')
                x = x.replace(':', ' : ')
                x = x.replace(';;', ';')
                x = x.replace(';', ' ; ')
                x = x.replace('===', '=')
                x = x.replace('==', '=')
                x = x.replace('    ', ' ')
                x = x.replace('   ', ' ')
                x = x.replace('  ', ' ')
                transcripts_cleaned.append(x)

            # get sentence length of all transcribes
            sentence_length = list()
            for sample in transcripts_cleaned:
                splits = sample.strip().split(' ')
                sentence_length.append(len(splits))

            # use only sentences with most common equal length( number of words)
            cnt = collections.Counter(sentence_length)
            most_common_length = cnt.most_common(1)[0][0]
            transcripts_cleaned_equal_length = list()
            for idx, val in enumerate(sentence_length):
                if val == most_common_length:
                    transcripts_cleaned_equal_length.append(transcripts_cleaned[idx])

            # count occurrence of each sentence element per index over all lines
            whole_dict = list()
            for idx in range(most_common_length):
                sentence_dict = dict()
                for sample in transcripts_cleaned_equal_length:
                    splits = sample.strip().split(' ')
                    try:
                        sentence_dict[splits[idx]] = sentence_dict[splits[idx]] + 1
                    except KeyError:
                        sentence_dict[splits[idx]] = 1
                whole_dict.append(sentence_dict)

            # get relative value of occurring word and it alternative
            samples_num = len(transcripts_cleaned_equal_length)
            for d in whole_dict:
                for key, value in d.items():
                    d[key] = value / samples_num
                d = collections.OrderedDict(sorted(d.items()))

            # save dictionaries
            name = line_prob.split('\\')
            outfile = open(save_path+'/'+name[-1][:-4]+'.p', 'wb')
            pickle.dump(whole_dict, outfile)
            outfile.close()
