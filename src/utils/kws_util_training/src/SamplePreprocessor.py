import random
import numpy as np
import cv2
import imutils
import peakutils
import math


def rotateImg(img):
	height, width = img.shape
	blackPixelFirstColumn = list()
	blackPixelSecondColumn = list()

	# count black pixels for two columns, width relative width
	for row in img[:, int(width / 2 - width / 4): int(width / 2 - width / 8)]:
		bincnt = np.bincount(row)
		blackPixelFirstColumn.append(bincnt[0])
	for row in img[:, int(width / 2 + width / 8): int(width / 2 + width / 4)]:
		bincnt = np.bincount(row)
		blackPixelSecondColumn.append(bincnt[0])

	# get row with black pixel width for both columns
	indexesBlackFirstColumn = peakutils.indexes((np.array(blackPixelFirstColumn)), thres=0.95, min_dist=1000)
	indexesBlackSecondColumn = peakutils.indexes((np.array(blackPixelSecondColumn)), thres=0.95, min_dist=1000)

	# calculate rotation angle
	lenAC = int(width / 2 + width / 4) - int(width / 2 - width / 4)
	lenBC = indexesBlackFirstColumn[0] - indexesBlackSecondColumn[0]
	lenAB = math.sqrt(lenAC ** 2 + lenBC ** 2)
	rotationAngle = int(math.degrees(math.asin(lenBC / lenAB)))
	# rotate image
	if rotationAngle != 0:
		img = imutils.rotate_bound(img, rotationAngle)

	return img


def preprocess(img, imgSize, dataAugmentation=False):
	# put img into target img of size imgSize, transpose for TF and normalize gray-values
	if img is None:
		img = np.zeros([imgSize[1], imgSize[0]])
	# increase dataset size by applying random stretches to the images
	if dataAugmentation:
		stretch = (random.random() - 0.5) # -0.5 .. +0.5
		wStretched = max(int(img.shape[1] * (1 + stretch)), 1) # random width, but at least 1
		img = cv2.resize(img, (wStretched, img.shape[0])) # stretch horizontally by factor 0.5 .. 1.5
	# remove text slope
	try:
		img = rotateImg(img)
	except:
		pass
	# create target image and copy sample image into it
	(wt, ht) = imgSize
	(h, w) = img.shape
	fx = w / wt
	fy = h / ht
	f = max(fx, fy)
	# scale according to f (result at least 1 and at most wt or ht)
	newSize = (max(min(wt, int(w / f)), 1), max(min(ht, int(h / f)), 1))
	img = cv2.resize(img, newSize)
	target = np.ones([ht, wt]) * 255
	target[0:newSize[1], 0:newSize[0]] = img

	# transpose for TF
	img = cv2.transpose(target)

	# normalize
	(m, s) = cv2.meanStdDev(img)
	m = m[0][0]
	s = s[0][0]
	img = img - m
	img = img / s if s>0 else img

	return img

