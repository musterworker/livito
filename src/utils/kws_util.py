import matplotlib.pyplot as plt
import numpy as np
import collections
import pickle
import itertools
import csv
import re
import os
import datetime
import cv2
import glob
# some handy functions to use along widgets
from IPython.display import display, clear_output
import ipywidgets as widgets
from tqdm import tqdm_notebook
import warnings
warnings.filterwarnings('ignore')
plt.switch_backend('agg')
#%matplotlib inline
#%%javascript
#IPython.OutputArea.auto_scroll_threshold = 9999

class ComputedValues:
    def __init__(self, trans, dicts, path_len, coords, names):
        self.all_pages_trans = trans
        self.all_dicts = dicts
        self.all_best_path_len = path_len
        self.line_coords = coords
        self.page_names = names

class Gui:
    def __init__(self, gui, out, out_text, out_general, searchID, current_page_idx, total_pages_found):
        self.gui = gui
        self.out = out
        self.out_text = out_text
        self.out_general = out_general
        self.searchID = searchID
        self.current_page_idx = current_page_idx
        self.total_pages_found = total_pages_found


def count_cases(best_path, upper, lower, other):
    string_merged = ''
    for i in best_path:
        string_merged += i
    for s in string_merged:
        # count upper and lower characters
        if s.isalpha():
            if s.isupper():
                upper += 1
            if s.islower():
                lower += 1
        # count all other characters, numbers and special signs
        else:
            other += 1
    return upper, lower, other


def initalize():
    all_pages_trans = list()
    all_dicts = list()
    all_best_path_len = list()
    line_coords = list()
    page_names = list()
    global case_list
    # iterate over all pages
    for file_name in tqdm_notebook(glob.glob('../data/preprocess_data/output/*')):
        # load coordinates from file
        try:
            infile = open(file_name + '/line_coords.p', 'rb')
            line_coords.append(pickle.load(infile))
            infile.close()
            fn = file_name.split('\\')[-1]
            page_names.append(file_name + '\\' + fn + '.tif')
        except:
            continue

        page_trans = list()
        dicts = list()
        best_path_len = list()
        upperCases = 0
        lowerCases = 0
        otherChars = 0
        # iterate over all lines of each page
        for trans_name in glob.glob(file_name + '/trans_dicts/*.p'):
            # load transcribed files
            infile = open(trans_name, 'rb')
            new_dict = pickle.load(infile)
            infile.close()
            # store values from transcribed files
            best_path = list(itertools.chain([[*x.keys()][0] for x in new_dict]))
            # count cases
            upperCases, lowerCases, otherChars = count_cases(best_path, upperCases, lowerCases, otherChars)
            best_path_len.append(len(' '.join(best_path).strip()))
            dicts.append(new_dict)
            merged_dict = list(itertools.chain(*[[*x.keys()] for x in new_dict]))
            page_trans.append(merged_dict)

        # save single page values to all_pages values
        case_list.append([file_name, upperCases, lowerCases, otherChars])
        all_pages_trans.append(page_trans)
        all_dicts.append(dicts)
        all_best_path_len.append(best_path_len)
    # save case statistics to file
    with open('../save/case_stats.csv', 'w') as case_file:
        writer = csv.writer(case_file, delimiter=';')
        writer.writerow(['File Path', 'Absolute Upper Characters', 'Absolute Lower Characters', 'Other Characters'])
        [writer.writerow(r) for r in case_list]

    initValues = ComputedValues(all_pages_trans, all_dicts, all_best_path_len, line_coords, page_names)
    return initValues


def run_search(initValues, search='Roku', search_type='in word'):
    search = search

    detected_pages = dict()
    detected_words = list()
    for page_idx, p_trans in enumerate(initValues.all_pages_trans):
        detected_lines = list()
        for line_idx, l_trans in enumerate(p_trans):
            text = ' '.join(l_trans)
            if search_type == 'in word':
                for m in re.finditer('\w*'+search+'\w*', text):
                    detected_lines.append(line_idx)
                    detected_words.append((page_idx, line_idx, m.group()))
            elif search_type == 'exact':
                for m in re.finditer(' '+search+' ', text):
                    detected_lines.append(line_idx)
                    detected_words.append((page_idx, line_idx, m.group().strip()))
            elif search_type == 'ending':
                for m in re.finditer('\w*'+search, text):
                    if len(m.group()) > len(search):
                        detected_lines.append(line_idx)
                        detected_words.append((page_idx, line_idx, m.group().strip()))
            elif search_type == 'begining':
                for m in re.finditer(search+'\w*', text):
                    if len(m.group()) > len(search):
                        detected_lines.append(line_idx)
                        detected_words.append((page_idx, line_idx, m.group().strip()))

        # uniquify list
        detected_lines = sorted(list(set(detected_lines)))
        # add detected lines to detected pages
        if len(detected_lines) > 0:
            detected_pages[page_idx] = detected_lines

    # get line and start/end-index of detected word
    detected_words_confidence = list()
    for detected_word in detected_words:
        p_idx = detected_word[0]
        l_idx = detected_word[1]
        word = detected_word[2]
        for w_idx, d in enumerate(initValues.all_dicts[p_idx][l_idx]):
            try:
                confidence = d[word]
                element_length = [len(list(d)[0]) for d in initValues.all_dicts[p_idx][l_idx]]
                #
                w_start = sum(element_length[0:w_idx])
                w_end = w_start + element_length[w_idx]
                # list containing: page, line, element index, word detected, word start, word end, confidence value
                if (p_idx, l_idx, w_idx, w_start, w_end, word, confidence) in detected_words_confidence:
                    continue
                else:
                    # 0 is for query zero
                    detected_words_confidence.append((0, p_idx, l_idx, w_idx, w_start, w_end, word, confidence))
            except KeyError:
                # needed to handle looking for keys in dict, which the dict does not contain
                pass

    # apply confidence criterium
    idx_to_remove = [idx for idx, val in enumerate(detected_words_confidence) if val[-1] < conf_slider.value]
    # remove indices from list in reversed order
    for i in idx_to_remove[::-1]:
        detected_words_confidence.pop(i)
    # get page indices
    page_indices = [detection[1] for detection in detected_words_confidence]
    page_indices = sorted(list(set(page_indices)))

    # get histogram for graph and overlay
    hist = collections.Counter([x[1] for x in detected_words_confidence])
    all_images = list()
    cnt = 0
    for detected_word_values in tqdm_notebook(detected_words_confidence):
        # skip 'None'-lines
        query_idx, page_idx, line_idx, w_idx, w_start, w_end, w_detected, confidence = detected_word_values
        if initValues.line_coords[page_idx][line_idx] == None:
            print('Skipped page {} line {}'.format(page_idx, line_idx))
            continue

        # load image and create overlay
        if cnt == 0:
            img = cv2.imread(initValues.page_names[page_idx])
            overlay = img.copy()
            # get coordinates from seperate lines
            fn_splits = initValues.page_names[page_idx].split('\\')
            infile = open(fn_splits[0] + '\\' + fn_splits[1] + '\\' + 'separating_seams.p', 'rb')  # sep_seams.p
            sep_seams = pickle.load(infile)
            infile.close()
            w = sep_seams.shape[0]
            first_row = np.zeros((w, 1), dtype=np.float64)
            last_row = np.ones((w, 1), dtype=np.float64) * (img.shape[0] - 10)
            new_stack = np.hstack((first_row, sep_seams, last_row))

            draw_multi_ellipses(overlay, detected_word_values, initValues.all_best_path_len, initValues.line_coords, new_stack)
            cnt += 1
        else:
            draw_multi_ellipses(overlay, detected_word_values, initValues.all_best_path_len, initValues.line_coords, new_stack)
            cnt += 1

        # save image with marked regions and reset counter
        if cnt == hist[page_idx]:
            opacity = 0.4
            cv2.addWeighted(overlay, opacity, img, 1 - opacity, 0, img)
            all_images.append(img)
            cnt = 0
    return all_images, page_indices, detected_words_confidence


def run_comparing_search(initValues, search=['blazena', 'blazenau'], search_type=['in word', 'exact']):
    search = search

    detected_pages = list([dict(), dict()])
    detected_words = list([list(), list()])

    for page_idx, p_trans in enumerate(initValues.all_pages_trans):
        detected_lines = list([list(), list()])
        for line_idx, l_trans in enumerate(p_trans):
            text = ' '.join(l_trans)
            # iterate over both querries per line
            for i in range(len(search)):
                if search_type[i] == 'in word':
                    for m in re.finditer('\w*' + search[i] + '\w*', text):
                        detected_lines[i].append(line_idx)
                        detected_words[i].append((page_idx, line_idx, m.group()))
                        # print(page_idx, line_idx, m.group())
                elif search_type[i] == 'exact':
                    for m in re.finditer(' ' + search[i] + ' ', text):
                        detected_lines[i].append(line_idx)
                        detected_words[i].append((page_idx, line_idx, m.group().strip()))
                elif search_type[i] == 'ending':
                    for m in re.finditer('\w*' + search[i], text):
                        if len(m.group()) > len(search[i]):
                            detected_lines[i].append(line_idx)
                            detected_words[i].append((page_idx, line_idx, m.group().strip()))
                elif search_type[i] == 'begining':
                    for m in re.finditer(search[i] + '\w*', text):
                        if len(m.group()) > len(search[i]):
                            detected_lines[i].append(line_idx)
                            detected_words[i].append((page_idx, line_idx, m.group().strip()))

        # uniquify list
        for i in range(len(search)):
            detected_lines[i] = sorted(list(set(detected_lines[i])))
        # add detected lines to detected pages
        for i in range(len(search)):
            if len(detected_lines[i]) > 0:
                detected_pages[i][page_idx] = detected_lines[i]

    # get line and start/end-index of detected word
    detected_words_confidence = list([list(), list()])
    for query_num in range(len(search)):
        for detected_word in detected_words[query_num]:
            p_idx = detected_word[0]
            l_idx = detected_word[1]
            word = detected_word[2]
            for w_idx, d in enumerate(initValues.all_dicts[p_idx][l_idx]):
                try:
                    confidence = d[word]
                    element_length = [len(list(d)[0]) for d in initValues.all_dicts[p_idx][l_idx]]
                    w_start = sum(element_length[0:w_idx])
                    w_end = w_start + element_length[w_idx]
                    # list containing: page, line, element index, word detected, word start, word end, confidence value
                    if (p_idx, l_idx, w_idx, w_start, w_end, word, confidence) in detected_words_confidence[i]:
                        continue
                    else:
                        detected_words_confidence[i].append(
                            (query_num, p_idx, l_idx, w_idx, w_start, w_end, word, confidence))
                except KeyError:
                    # needed to handle looking for keys in dict, which the dict does not contain
                    pass

    # apply confidence criterium
    idx_to_remove = list([list(), list()])
    idx_to_remove[0] = [idx for idx, val in enumerate(detected_words_confidence[0]) if val[-1] < conf_slider_1.value]
    idx_to_remove[1] = [idx for idx, val in enumerate(detected_words_confidence[1]) if val[-1] < conf_slider_2.value]
    # remove indices from list in reversed order
    for i in range(len(search)):
        for j in idx_to_remove[i][::-1]:
            detected_words_confidence[i].pop(j)
    # get page indices
    page_indices = list([list(), list()])
    for i in range(len(search)):
        page_indices[i] = [detection[1] for detection in detected_words_confidence[i]]
    page_indices = sorted(list(set(page_indices[0] + page_indices[1])))
    # sort by page number
    detected_words_confidence = sorted(detected_words_confidence[0] + detected_words_confidence[1])
    detected_words_confidence = sorted(detected_words_confidence, key=lambda x: int(x[1]))
    # get histogram for overlay
    hist = collections.Counter([x[1] for x in detected_words_confidence])
    all_images = list()
    cnt = 0
    for detected_word_values in tqdm_notebook(detected_words_confidence):
        # skip 'None'-lines
        query_num, page_idx, line_idx, w_idx, w_start, w_end, w_detected, confidence = detected_word_values
        if initValues.line_coords[page_idx][line_idx] == None:
            print('Skipped page {} line {}'.format(page_idx, line_idx))
            continue

        # load image and create overlay
        if cnt == 0:
            img = cv2.imread(initValues.page_names[page_idx])
            overlay = img.copy()
            # get coordinates from seperate lines
            fn_splits = initValues.page_names[page_idx].split('\\')
            infile = open(fn_splits[0] + '\\' + fn_splits[1] + '\\' + 'separating_seams.p', 'rb')
            sep_seams = pickle.load(infile)
            infile.close()
            w = sep_seams.shape[0]
            # first and last row needed for ellipse drawing algorithm
            first_row = np.zeros((w, 1), dtype=np.float64)
            last_row = np.ones((w, 1), dtype=np.float64) * (img.shape[0] - 10)
            new_stack = np.hstack((first_row, sep_seams, last_row))

            draw_multi_ellipses(overlay, detected_word_values, initValues.all_best_path_len, initValues.line_coords, new_stack)
            cnt += 1
        else:
            draw_multi_ellipses(overlay, detected_word_values, initValues.all_best_path_len, initValues.line_coords, new_stack)
            cnt += 1
        # save image with marked regions and reset counter
        if cnt == hist[page_idx]:
            opacity = 0.4
            cv2.addWeighted(overlay, opacity, img, 1 - opacity, 0, img)
            all_images.append(img)
            cnt = 0

    return all_images, page_indices, detected_words_confidence


def draw_multi_ellipses(overlay, detected_word_values, all_best_path_len, line_coords, new_stack):
    query_num, page_idx, line_idx, w_idx, w_start, w_end, w_detected, confidence = detected_word_values

    if query_num == 0:
        color = (0, 255, 255)
    elif query_num == 1:
        color = (0, 0, 255)

    line_length = line_coords[page_idx][line_idx][1] - line_coords[page_idx][line_idx][0]
    char_len = int(line_length / all_best_path_len[page_idx][line_idx]) + 5  # 5 is an arbitrary value, which work well
    if line_idx == 0:
        ellipse_height = int(max(new_stack[:, line_idx]) - min(new_stack[:, [line_idx]]))
    else:
        ellipse_height = min(int(min(new_stack[:, line_idx]) - min(new_stack[:, line_idx - 1])), 100)
        if ellipse_height < 0:
            ellipse_height = 100
    ellipse_width = int((w_end - w_start) * char_len * 2)

    if ellipse_width > 400:
        ellipse_width //= 2
    # get ellipse center coordinates, slight shift downwards +20 and to the right +100, showed good results
    center_x = int((w_start + (w_end - w_start) / 2) * char_len) + line_coords[page_idx][line_idx][0] + 100
    center_y = int(max(new_stack[w_start:w_end, line_idx])) + 20
    # handle case of line zero
    if line_idx == 0:
        center_y = int(min(new_stack[w_start:w_end, line_idx + 1])) - 60
        ellipse_height = int(max(new_stack[:, line_idx + 1]) - min(new_stack[:, [line_idx + 1]]))
    # draw ellipse
    cv2.ellipse(overlay, center=(center_x, center_y), axes=(ellipse_width, ellipse_height + 4),
                angle=0, startAngle=0, endAngle=360, color=color, thickness=-1)


# gui functions
def on_button_clicked_next(_):
    # linking function with output
    global current_page_idx
    img_index = 0
    # increase page index
    if deleted_clicked == False:
        if current_page_idx < GuiValues.total_pages_found-1:
            current_page_idx += 1
    if deleted_clicked == True:
        if current_page_idx < len(remaining_pages)-1:
            current_page_idx = remaining_pages.index(GuiValues.searchID.value) + 1
    # print textual results
    with GuiValues.out_text:
        clear_output()
        print('\n''\n')
        print('Page  Line  Word {:^19} Conf'.format('Found'))
        for idx, res in enumerate(detected_words_confidence):
            if deleted_clicked == True:
                if res[1] == remaining_pages[current_page_idx]:
                    print('{:^4}  {:^4}  {:^4} {:^19} {:^4.2f}'.format(res[1], res[2], res[3], res[-2], res[-1]))
                    img_index = page_indices.index(remaining_pages[current_page_idx])
                    # set dropdown value
                    GuiValues.searchID.value = res[1]
            else:
                if res[1] == page_indices[current_page_idx]:
                    print('{:^4}  {:^4}  {:^4} {:^19} {:^4.2f}'.format(res[1], res[2], res[3], res[-2], res[-1]))
                    img_index = page_indices.index(page_indices[current_page_idx])
                    # set dropdown value
                    GuiValues.searchID.value = res[1]
    # print graphical results
    with GuiValues.out:
        clear_output()
        img_str = cv2.imencode('.png', all_images[current_page_idx])[1].tostring()
        display(widgets.Image(value=img_str, format='png', width=500, height=600, ))


def on_button_clicked_previous(_):
    # linking function with output
    global current_page_idx
    img_index = 0
    # print textual results
    with GuiValues.out_text:
        clear_output()
        print('\n''\n')
        print('Page  Line  Word {:^19} Conf'.format('Found'))
        # set page index corresponding to search type
        if deleted_clicked == True:
            if current_page_idx > 0:
                current_page_idx = remaining_pages.index(GuiValues.searchID.value) - 1
        else:
            if current_page_idx > 0:
                current_page_idx -= 1
        # iterate over detected results to find those previous to the current one
        for idx, res in enumerate(detected_words_confidence):
            if deleted_clicked == True:
                if res[1] == remaining_pages[current_page_idx]:
                    print('{:^4}  {:^4}  {:^4} {:^19} {:^4.2f}'.format(res[1], res[2], res[3], res[-2], res[-1]))
                    img_index = page_indices.index(remaining_pages[current_page_idx])
                    # set dropdown value
                    GuiValues.searchID.value = res[1]
            else:
                if res[1] == page_indices[current_page_idx]:
                    print('{:^4}  {:^4}  {:^4} {:^19} {:^4.2f}'.format(res[1], res[2], res[3], res[-2], res[-1]))
                    img_index = page_indices.index(page_indices[current_page_idx])
                    # set dropdown value
                    GuiValues.searchID.value = res[1]
    with GuiValues.out:
        clear_output()
        # convert numpy array to bytestring for processing with ipython
        img_str = cv2.imencode('.png', all_images[current_page_idx])[1].tostring()
        display(widgets.Image(value=img_str, format='png', width=500, height=600, ))


def on_button_clicked_goto(_):
    # linking function with output
    global current_page_idx, last_clicked
    if deleted_clicked == True:
        current_page_idx = remaining_pages.index(GuiValues.searchID.value)
    else:
        current_page_idx = page_indices.index(GuiValues.searchID.value)
    current_page_idx = page_indices.index(GuiValues.searchID.value)
    # print textual results
    with GuiValues.out_text:
        clear_output()
        print('\n''\n')
        print('Page  Line  Word {:^19} Conf'.format('Found'))
        for res in detected_words_confidence:
            if res[1] == GuiValues.searchID.value:
                print('{:^4}  {:^4}  {:^4} {:^19} {:^4.2f}'.format(res[1], res[2], res[3], res[-2], res[-1]))

    # print graphical results
    with GuiValues.out:
        clear_output()
        # convert numpy array to bytestring for processing with ipython
        img_str = cv2.imencode('.png', all_images[current_page_idx])[1].tostring()
        display(widgets.Image(value=img_str, format='png', width=500, height=600, ))


def on_button_clicked_search(_):
    # run search
    global all_images
    global page_indices
    global detected_words_confidence
    global searchedfor
    searchedfor = search_box.value
    with state_display:
        clear_output()
        print('Searching...')
        all_images, page_indices, detected_words_confidence = run_search(initValues, search_box.value, search_type.value)
        clear_output()
        print('Finished!!!')
        # log results
        log = open('../save/log.txt', mode='a', encoding='utf-8-sig')
        now = datetime.datetime.now()
        log.writelines(
            '{} {} {} {} {} {} \n'.format(now.strftime("%Y-%m-%d %H:%M:%S"), search_box.value, search_type.value,
                                          conf_slider.value, len(detected_words_confidence), len(all_images)))
        log.close()


def on_button_clicked_comparing_search(_):
    # run search
    global all_images
    global page_indices
    global detected_words_confidence
    global searchedfor
    searchedfor = search_box_1.value+'/'+search_box_2.value
    with state_display_1:
        clear_output()
        print('Searching...')
        all_images, page_indices, detected_words_confidence = run_comparing_search(initValues, [search_box_1.value, search_box_2.value], [search_type_1.value, search_type_2.value])
        clear_output()
        print('Finished!!!')
        # log results
        log = open('../save/log.txt', mode='a', encoding='utf-8-sig')
        now = datetime.datetime.now()
        log.writelines('{} {} {} {} {} {} \n'.format(now.strftime("%Y-%m-%d %H:%M:%S"), searchedfor, str(search_type_1.value)+'/'+str(search_type_2.value), str(conf_slider_1.value)+'/'+str(conf_slider_2.value), len(detected_words_confidence), len(all_images)))
        log.close()


def on_button_clicked_delete(_):
    # get deleted results
    global deleted_elements
    global remaining_pages
    global deleted_clicked
    deleted_clicked = True
    remaining_pages = list()

    for i, res in enumerate(outputs):
        if res[1].value == True:
            deleted_elements.append(i)
        else:
            remaining_pages.append(res[3])
        # sort by unique elements
    deleted_elements = sorted(list(set(deleted_elements)))
    remaining_pages = sorted(list(set(remaining_pages)))
    # get in output context
    with GuiValues.out_general:
        clear_output()
        num_outputs = collections.Counter(x[2] for x in outputs)
        print('Found {} hits on {} pages for searching \"{}\".'.format(
            len(detected_words_confidence) - len(deleted_elements), len(remaining_pages), searchedfor))
        print('\n')
        save_button = widgets.Button(description='Export Results', disabled=False, button_style='info')
        save_button.on_click(on_button_clicked_save)
        delete_button = widgets.Button(description='Delete Results', disabled=False, button_style='info')
        delete_button.on_click(on_button_clicked_delete)
        display(widgets.HBox([widgets.HTML(
            value="<table><tr>"
                  "<td style='width:240px; text-align:center; font-weight:bold'>{}</td>"
                  "<td style='width:100px; text-align:center; font-weight:bold'>{}</td>"
                  "<td style='width:180px; text-align:center; font-weight:bold'>{}</td></tr></table>".format(
                'Filename', 'Page ID', 'Detected Word')), delete_button, save_button]))
        # print cleaned results in general info
        for idx, res in enumerate(outputs):
            if idx in deleted_elements:
                continue
            else:
                if len(num_outputs) == 2:
                    if idx == 0:
                        display(widgets.HTML("<b>Results for the first Query</b>"))
                    elif idx == num_outputs[0]:
                        display(widgets.HTML("<b>Results for the second Query</b>"))
                display(widgets.HBox([res[0], res[1]]))

    # set new values for graphics dropdown box
        GuiValues.searchID.options = remaining_pages
    try:
        GuiValues.searchID.value = remaining_pages[0]
        # here might be a bug to fix...displaying wrong potentially
        # an image which was deleted and showing wrong dropdown value
    # in case all results got deleted
    except IndexError:
        GuiValues.searchID.value = None

    # redraw statistics
    draw_statistics(state='draw')


def on_button_clicked_save(_):
    # get date time for unique filename
    now = datetime.datetime.now()
    save_path = '../save/' + now.strftime("%Y-%m-%d-%H-%M-%S") + '/'
    os.makedirs(save_path)

    f = open(save_path + 'results.txt', 'w', encoding='utf-8-sig')
    if deleted_clicked == True:
         f.write('Found {} hits on {} pages\n'.format(len(detected_words_confidence)-len(deleted_elements), len(remaining_pages)))
    else:
        f.write('Found {} hits on {} pages\n'.format(len(detected_words_confidence), total_pages_found))
    f.write("Filename;")
    f.write("PageID;")
    f.write("Detected Word\n")
    num_outputs = collections.Counter(x[2] for x in outputs) 
    if deleted_clicked == True:
        for idx, res in enumerate(outputs):
            if idx in deleted_elements:
                continue
            else:
                if len(num_outputs) == 2:
                    if idx == 0:
                        f.write('Results for the first Query\n')
                    elif idx == num_outputs[0]:
                        f.write('Results for the second Query\n')
                _, fn, _, ID, _, w = res[0].value.split(' ')
                fn = fn[20:-8]
                ID = ID[19:-8]
                w = w[19:-18]
                f.write(fn+';'+ID+';'+w+'\n')
    else:
        for idx, res in enumerate(outputs):
            if len(num_outputs) == 2:
                if idx == 0:
                    f.write('Results for the first Query\n')
                elif idx == num_outputs[0]:
                    f.write('Results for the second Query\n')
            _, fn, _, ID, _, w = res[0].value.split(' ')
            fn = fn[20:-8]
            ID = ID[19:-8]
            w = w[19:-18]
            f.write(fn+';'+ID+';'+w+'\n')

    f.close()
    # save statistics
    draw_statistics(state='save', save_path=save_path)


def draw_char_stats(state='draw', save_path='../save/'):
    count_upper = [x[1] for x in case_list]
    count_lower = [x[2] for x in case_list]
    count_other = [x[3] for x in case_list]

    with char_graph_abs:
        clear_output()
        fig = plt.figure(figsize=(20, 8))
        ax = fig.add_subplot(111)
        ax.bar(range(len(count_upper)), count_upper, alpha=0.7, color='g', label='Upper Case')
        ax.bar(range(len(count_lower)), count_lower, alpha=0.3, color='b', label='Lower Case')
        ax.bar(range(len(count_other)), count_other, alpha=0.3, color='r', label='Other Chars')
        plt.gca().set(title='Absolute Histogram of Character Cases ', ylabel='absolute', xlabel='PageID')
        plt.legend()
        if state == 'save':
            plt.savefig(save_path + 'char_cases_abs.png', dpi=300)
            plt.show()
        elif state == 'draw':
            plt.show()

    with char_graph_rel:
        clear_output()
        fig = plt.figure(figsize=(20, 8))
        ax = fig.add_subplot(111)
        s4 = np.array(count_upper) + np.array(count_lower) + np.array(count_other)
        s1r = np.array(count_upper) / s4
        s2r = np.array(count_lower) / s4
        s3r = np.array(count_other) / s4
        ax.bar(range(s1r.shape[0]), s1r, alpha=0.7, color='g', label='Upper Case')
        ax.bar(range(s2r.shape[0]), s2r, alpha=0.3, color='b', label='Lower Case')
        ax.bar(range(s3r.shape[0]), s3r, alpha=0.3, color='r', label='Other Chars')
        plt.gca().set(title='Relative Histogram of Character Cases ', ylabel='relative', xlabel='PageID')
        plt.legend()
        if state == 'save':
            plt.savefig(save_path + 'char_cases_rel.png', dpi=300)
            plt.show()
        elif state == 'draw':
            plt.show()


def draw_statistics(state='draw', save_path='../save/'):
    num_outputs = collections.Counter(x[2] for x in outputs)
    if deleted_clicked == True:
        remaining_words = list()
        for idx, res in enumerate(outputs):
            if idx in deleted_elements:
                continue
            else:
                remaining_words.append((res[2], res[3], res[-1]))
                remaining_num_outpus = collections.Counter(x[0] for x in remaining_words)
    # distribution of query(s) over document
    with out_general_graph_1:
        clear_output()
        if deleted_clicked == False:
            if len(num_outputs) == 2:
                hist1 = collections.Counter([x[3] for x in outputs[:num_outputs[0]]])
                hist2 = collections.Counter([x[3] for x in outputs[num_outputs[0]:]])
            else:
                hist = collections.Counter([x[3] for x in outputs])
        else:
            remaining_words = list()
            for idx, res in enumerate(outputs):
                if idx in deleted_elements:
                    continue
                else:
                    remaining_words.append(res[3])
            if len(num_outputs) == 2:
                hist1 = collections.Counter(remaining_words[:remaining_num_outpus[0]])
                hist2 = collections.Counter(remaining_words[remaining_num_outpus[0]:])
            else:
                hist = collections.Counter(remaining_words)

        fig = plt.figure(figsize=(20, 8))
        ax = fig.add_subplot(111)
        if len(num_outputs) == 2:
            X1 = list(hist1.keys())
            X2 = list(hist2.keys())
            # shift first query results by 0.3 so they do not overlap
            ax.bar(np.array(X1) + 0.3, list(hist1.values()), 0.5, label=search_box_1.value)
            ax.bar(np.array(X2) - 0.3, list(hist2.values()), 0.5, label=search_box_2.value)
            plt.legend()
        else:
            ax.bar(list(hist.keys()), list(hist.values()))

        plt.title('Histogram of search results')
        plt.ylabel('Hits')
        plt.xlabel('PageID')
        if state == 'save':
            plt.savefig(save_path + 'distribution.png', dpi=300)
            plt.show()
        elif state == 'draw':
            plt.show()

    # histogram of word alternatives found on 'in word' search
    with out_general_graph_2:
        clear_output()
        if deleted_clicked == False:
            if len(num_outputs) == 2:
                wf1 = collections.Counter([x[-1] for x in outputs[:num_outputs[0]]])
                wf2 = collections.Counter([x[-1] for x in outputs[num_outputs[0]:]])
            else:
                wf = collections.Counter([x[-2] for x in detected_words_confidence])
        else:
            remaining_words = list()
            for idx, res in enumerate(outputs):
                if idx in deleted_elements:
                    continue
                else:
                    remaining_words.append(res[-1])
            if len(num_outputs) == 2:
                wf1 = collections.Counter(remaining_words[:remaining_num_outpus[0]])
                wf2 = collections.Counter(remaining_words[remaining_num_outpus[0]:])
            else:
                wf = collections.Counter(remaining_words)
        if len(num_outputs) == 2:
            most_common1 = wf1.most_common(20)
            performance1 = [v[1] for v in most_common1][::-1]
            objects1 = [v[0] for v in most_common1][::-1]
            y_pos1 = np.arange(len(objects1))
            fig = plt.figure(figsize=(20, 8))
            ax = fig.add_subplot(121)
            ax.barh(y_pos1, performance1, align='center', alpha=0.5)
            plt.yticks(y_pos1, objects1)
            plt.xlabel('Number of apperances')
            plt.title('In word detection distribution query 1')

            # plot two
            most_common2 = wf2.most_common(20)
            performance2 = [v[1] for v in most_common2][::-1]
            objects2 = [v[0] for v in most_common2][::-1]
            y_pos2 = np.arange(len(objects2))
            ax = fig.add_subplot(122)
            ax.barh(y_pos2, performance2, align='center', alpha=0.5)
            plt.yticks(y_pos2, objects2)
            plt.xlabel('Number of apperances')
            plt.title('In word detection distribution query 2')

        else:
            most_common = wf.most_common(20)
            performance = [v[1] for v in most_common][::-1]
            objects = [v[0] for v in most_common][::-1]
            y_pos = np.arange(len(objects))
            fig = plt.figure(figsize=(20, 8))
            ax = fig.add_subplot(111)
            ax.barh(y_pos, performance, align='center', alpha=0.5)

            plt.yticks(y_pos, objects)
            plt.xlabel('Number of apperances')
            plt.title('In word detection distribution')
        # draw/save histogram(s)
        if state == 'save':
            plt.savefig(save_path + 'histogram.png', dpi=300)
            plt.show()
        elif state == 'draw':
            plt.show()


def setup_gui(initValues):
    # widgets
    button_next = widgets.Button(description='Next')
    button_previous = widgets.Button(description='Previous')
    button_goto = widgets.Button(description='Go to')

    # simple outputs
    out = widgets.Output()
    out_text = widgets.Output()

    # global variables
    global total_pages_found
    total_pages_found = len(all_images)
    current_page_idx = 0
    global deleted_elements
    deleted_clicked = False
    deleted_elements = list()
    global outputs
    outputs = list()
    # set up general info tab
    out_general = widgets.Output(layout={'width': '1000px'})

    # print textual results
    with out_general:
        clear_output()
        print('Found {} hits on {} pages for searching \"{}\".\n'.format(len(detected_words_confidence), total_pages_found, searchedfor))
    for res in tqdm_notebook(sorted(detected_words_confidence)):
        fn = initValues.page_names[res[0]].split('\\')[2]
        with out_general:
            out_search = widgets.HTML(value="<table><tr>"
                                            "<td style='width:240px'>{}</td>"
                                            "<td style='width:100px; text-align:center'>{}</td>"
                                            "<td style='width:180px; text-align:center'>{}</td></tr></table>".format(fn, res[1], res[6]))
            out_check = widgets.Checkbox()
        outputs.append([out_search, out_check, res[0], res[1], res[2], res[3], res[6]])

    # display search results if there are any
    if len(outputs) > 0:
        with out_general:
            save_button = widgets.Button(description='Export Results', disabled=False, button_style='info')
            save_button.on_click(on_button_clicked_save)
            delete_button = widgets.Button(description='Delete Results', disabled=False, button_style='info')
            delete_button.on_click(on_button_clicked_delete)
            display(widgets.HBox([widgets.HTML(value="<table><tr><td style='width:240px; text-align:center; font-weight:bold'>{}</td>"
                                                     "<td style='width:100px; text-align:center; font-weight:bold'>{}</td>"
                                                     "<td style='width:180px; text-align:center; font-weight:bold'>{}</td></tr></table>".format(
                    'Filename', 'Page ID', 'Detected Word')), delete_button, save_button]))
            num_outputs = collections.Counter(x[2] for x in outputs)
        for idx, res in tqdm_notebook(enumerate(outputs)):
            if len(num_outputs) == 2:
                if idx == 0:
                    with out_general:
                        display(widgets.HTML("<b>Results for the first Query</b>"))
                elif idx == num_outputs[0]:
                    with out_general:
                        display(widgets.HTML("<b>Results for the second Query</b>"))
            with out_general:
                display(widgets.HBox([res[0], res[1]]))

    # clear progress bars
    clear_output()
    draw_statistics()

    # linking button and function together using a button's method
    button_next.on_click(on_button_clicked_next)
    button_previous.on_click(on_button_clicked_previous)
    button_goto.on_click(on_button_clicked_goto)

    # displaying button and its output together
    if len(page_indices) == 0:
        searchID = widgets.Dropdown(layout={'width': '100px'}, disabled=False)
    else:
        searchID = widgets.Dropdown(options=page_indices, value=page_indices[0], layout={'width': '100px'},
                                    disabled=False)

    general = widgets.HBox([out_general])
    graphics = widgets.HBox([widgets.VBox([widgets.HBox([button_previous, searchID, button_goto, button_next]), out]),
                             out_text])
    statistics = widgets.VBox([out_general_graph_1, out_general_graph_2])
    char_stats = widgets.VBox([char_graph_rel, char_graph_abs])

    tab_contents = ['General Info', 'Graphics', 'Statistics', 'Character Stats']
    gui = widgets.Tab(layout={'height': '1000px'})
    gui.children = [general, graphics, statistics, char_stats]
    gui.set_title(0, tab_contents[0])
    gui.set_title(1, tab_contents[1])
    gui.set_title(2, tab_contents[2])
    gui.set_title(3, tab_contents[3])
    GuiValues = Gui(gui, out, out_text, out_general, searchID, current_page_idx, total_pages_found)

    return GuiValues


def show_results():
    global GuiValues
    GuiValues = setup_gui(initValues)
    draw_char_stats()
    return GuiValues.gui

# global variables
log = open('../save/log.txt', mode='a', encoding='utf-8-sig')
all_images = list()
page_indices = list()
total_pages_found = 0
case_list = list()
detected_words_confidence = list()
draw_chars = True
deleted_clicked = False
searchedfor = 'Roku'
out_general_graph_1 = widgets.Output()
out_general_graph_2 = widgets.Output()
char_graph_rel = widgets.Output()
char_graph_abs = widgets.Output()
# search gui
search_box = widgets.Text(value='blazena', placeholder='Type something', description='Search for:', disabled=False)
search_type = widgets.RadioButtons(options=['exact', 'in word', 'ending', 'begining'], value='exact', description='Search type:', disabled=False, layout={'width': '220px'})
conf_slider = widgets.FloatSlider(value=1.0, min=0, max=1.0, step=0.05, description='min. Conf:', disabled=False, continuous_update=False, orientation='horizontal', readout=True, readout_format='.2f',)
button_search = widgets.Button(description='Search')
state_display = widgets.Output(layout={'width': '200px'})
button_search.on_click(on_button_clicked_search)
search_gui_simple = widgets.HBox([search_box, search_type, conf_slider, button_search, state_display],layout={'height': '120px'})
# additional search gui for comparison
# box 1
search_box_1 = widgets.Text(value='blazena', placeholder='Type something', description='Search for:', disabled=False, layout={'width': '240px'})
search_type_1 = widgets.RadioButtons(options=['exact', 'in word', 'ending', 'begining'], value='exact', description='Search type:', disabled=False, layout={'width': '180px'})
conf_slider_1 = widgets.FloatSlider(value=1.0, min=0, max=1.0, step=0.05, description='min. Conf:', disabled=False, continuous_update=False, orientation='horizontal', readout=True, readout_format='.2f', layout={'width': '250px'})
button_search_1 = widgets.Button(description='Search')
state_display_1 = widgets.Output(layout={'width': '110px'})
button_search_1.on_click(on_button_clicked_comparing_search)
search_gui_simple_1 = widgets.HBox([search_box_1, search_type_1, conf_slider_1, button_search_1, state_display_1], layout={'height': '120px'})
#box 2
search_box_2 = widgets.Text(value='blazenau', placeholder='Type something', description='Compare:', disabled=False, layout={'width': '240px'})
search_type_2 = widgets.RadioButtons(options=['exact', 'in word', 'ending', 'begining'], value='exact', description='Search type:', disabled=False, layout={'width': '180px'})
conf_slider_2 = widgets.FloatSlider(value=1.0, min=0, max=1.0, step=0.05, description='min. Conf:', disabled=False, continuous_update=False, orientation='horizontal', readout=True, readout_format='.2f', layout={'width': '250px'})
search_gui_simple_2 = widgets.HBox([search_box_2, search_type_2, conf_slider_2], layout={'height': '120px'})
comparing_gui = widgets.VBox([search_gui_simple_1, search_gui_simple_2])
# merge to tabular view
tab_contents_search_gui = ['Simple Search', 'Comparing Search']
search_gui = widgets.Tab(layout={'height': '300px'})
search_gui.children = [search_gui_simple, comparing_gui]
search_gui.set_title(0, tab_contents_search_gui[0])
search_gui.set_title(1, tab_contents_search_gui[1])

# start calculation
initValues = initalize()
GuiValues = setup_gui(initValues)
draw_chars = True
