import cv2
import numpy as np
from scipy import misc
from skimage.measure import regionprops, label
import glob
import tensorflow as tf
from tqdm import tqdm_notebook
# some handy functions to use along widgets
from IPython.display import display, clear_output
import ipywidgets as widgets
import warnings
warnings.filterwarnings("ignore")
#%matplotlib widget
#%%javascript
#IPython.OutputArea.auto_scroll_threshold = 9999;
#tf.logging.set_verbosity(tf.logging.INFO)
tf.logging.set_verbosity(tf.logging.ERROR)


def check_prop_size(props):
    prop_list = list()
    if len(props) > 0:
        for prop in props:
            if prop.area > area_thres:
                prop_list.append(prop)
            else:
                pass
                
    return prop_list


def draw_image(img):
    with out1:
        clear_output(wait=True)
        # convert numpy array to bytestring for processing with ipython
        img_str = cv2.imencode('.png', img)[1].tostring()
        display(widgets.Image(value=img_str, format='png', width=500, height=600))


def init_gui():
    global current_idx
    if layer_type.label == 'All':
        overlayBlank = np.zeros(all_output_props[current_idx][-1].shape, dtype=np.uint8)
        for color_idx, prop in enumerate(all_output_props[current_idx][0]):
            if len(prop) > 0:
                color = colors[color_idx]
                for i in prop:
                    for x, y in i.coords:
                        overlayBlank[x, y] = color
            else:
                continue
    else:
        layer_content = [len(x) for x in all_output_props[current_idx][0]]
        lt = layer_type.options.index(layer_type.label) - 1 
        overlayBlank = np.zeros(all_output_props[current_idx][-1].shape, dtype=np.uint8)
        color = colors[lt]
        prop = all_output_props[current_idx][0][lt]
        if len(prop) > 0:
            for i in prop:
                if i.area > area_thres:
                    for x, y in i.coords:
                        overlayBlank[x, y] = color

    dst = cv2.addWeighted(overlayBlank, 0.5, all_output_props[current_idx][-1], 1, 0)
    draw_image(dst)


def on_button_clicked_next(_):
    global current_idx

    if layer_type.label == 'All':
        if current_idx == len(all_output_props)-1:
            return
        else:
            current_idx += 1    
        overlayBlank = np.zeros(all_output_props[current_idx][-1].shape, dtype=np.uint8)
        for color_idx, prop in enumerate(all_output_props[current_idx][0]):
            if len(prop) > 0:
                color = colors[color_idx]
                for i in prop:
                    for x, y in i.coords:
                        overlayBlank[x, y] = color
            else:
                continue
    else:
        # check if current index has active layer
        if current_idx == len(all_output_props)-1:
            return
        else:
            current_idx += 1 

        layer_content = [len(x) for x in all_output_props[current_idx][0]]
        lt = layer_type.options.index(layer_type.label) - 1 
        while layer_content[lt] == 0:
            if current_idx == len(all_output_props)-1:
                current_idx -= 1
                return
            else:
                current_idx += 1 
                layer_content = [len(x) for x in all_output_props[current_idx][0]]

        overlayBlank = np.zeros(all_output_props[current_idx][-1].shape, dtype=np.uint8)
        color = colors[lt]
        prop = all_output_props[current_idx][0][lt]
        if len(prop) > 0:
            for i in prop:
                for x, y in i.coords:
                    overlayBlank[x, y] = color

    dst = cv2.addWeighted(overlayBlank, 0.5, all_output_props[current_idx][-1], 1, 0)
    draw_image(dst)


def on_button_clicked_previous(_):
    global current_idx
    if layer_type.label == 'All':
        if current_idx == 0:
            return
        else:
            current_idx -= 1  
        overlayBlank = np.zeros(all_output_props[current_idx][-1].shape, dtype=np.uint8)
        for color_idx, prop in enumerate(all_output_props[current_idx][0]):
            if len(prop) > 0:
                color = colors[color_idx]
                for i in prop:
                    for x, y in i.coords:
                        overlayBlank[x, y] = color
            else:
                continue
    else:
        if current_idx == 0:
            return
        else:
            current_idx -= 1 
        # check if current index has active layer
        layer_content = [len(x) for x in all_output_props[current_idx][0]]
        lt = layer_type.options.index(layer_type.label) - 1 
        while layer_content[lt] == 0:
            if current_idx == 0:
                break
            else:
                current_idx -= 1 
                layer_content = [len(x) for x in all_output_props[current_idx][0]]
                
        overlayBlank = np.zeros(all_output_props[current_idx][-1].shape, dtype=np.uint8)
        color = colors[lt]
        prop = all_output_props[current_idx][0][lt]
        if len(prop) > 0:
            for i in prop:
                for x, y in i.coords:
                    overlayBlank[x, y] = color

    dst = cv2.addWeighted(overlayBlank, 0.5, all_output_props[current_idx][-1], 1, 0)
    draw_image(dst)


def on_trait_changed_layers(_):
    overlayBlank = np.zeros(all_output_props[current_idx][-1].shape, dtype=np.uint8)
    # draw all layers
    if layer_type.label == 'All':
        for color_idx, prop in enumerate(all_output_props[current_idx][0]):
            if len(prop) > 0:
                color = colors[color_idx]
                for i in prop:
                    for x, y in i.coords:
                        overlayBlank[x, y] = color
            else:
                continue
    # draw specific layers
    else:
        lt = layer_type.options.index(layer_type.label)
        color = colors[lt-1]
        prop = all_output_props[current_idx][0][lt-1]
        if len(prop) > 0:
            for i in prop:
                for x,y in i.coords:
                    overlayBlank[x, y] = color

    dst = cv2.addWeighted(overlayBlank, 0.5, all_output_props[current_idx][-1], 1, 0)
    draw_image(dst)


# global variables
scale = 0.25
# individual thres values for different layers, indicating the minimum confidence level, that is used to validate
# a layer
conf_thresh = [0.55, 0.4, 0.55]
restore_ckt_path = './utils/revision_model/model90'
all_output_props = list()
# global variables for GUI
current_idx = 0
colors = [[0, 0, 255], [0, 255, 0], [255, 0, 0]]
area_thres = 1050
with tf.Session() as sess:
    # restore model and its variables
    saver = tf.train.import_meta_graph(restore_ckt_path+'.meta')
    saver.restore(sess, restore_ckt_path)
    for im_path in tqdm_notebook(glob.glob('../data/preprocess_data/output/*/*.tif')):
        img = cv2.imread(im_path, 0)
        img3d = cv2.imread(im_path)
        img3d = np.expand_dims(misc.imresize(img3d, scale, interp='bicubic'),0)
        img3d = cv2.cvtColor(img3d[0], cv2.COLOR_BGR2RGB)
        img3d = cv2.cvtColor(img3d, cv2.COLOR_RGB2BGR)
        
        input_data = np.expand_dims(misc.imresize(img, scale, interp='bicubic'),2)
        input_data = np.reshape(input_data, (1, input_data.shape[0], input_data.shape[1], 1))
        
        out = sess.run('output:0', feed_dict={'inImg:0': input_data})
        # analyse output for detected areas
        all_props = list()
        for i in range(3):
            im = out[0, :, :, i]
            im[im > conf_thresh[i]] = 1
            im[im <= conf_thresh[i]] = 0
            kernel = np.ones((5, 5), np.uint8)
            dilation = cv2.dilate(im, kernel, iterations=5)
            labels = label(dilation)
            props = regionprops(labels)
            props = check_prop_size(props)
            all_props.append(props)
        # TODO: keep only images and outputs if there was something detected on the page
        if sum([len(element) for element in all_props]) > 0:
            all_output_props.append([all_props, img3d])

sess.close()


# widget layout
out1 = widgets.Output(layout={'height': '800px'})
# out2 = widgets.Output()
button_next = widgets.Button(description='Next')
button_previous = widgets.Button(description='Previous')
type_selector = widgets.Dropdown(options=[('All', 0), ('Crossed out', 1), ('Changed', 2), ('Inserted', 3)], value=0, description='Sort by:', layout={'width': '200px'})
layer_type = widgets.ToggleButtons(options=['All', 'Crossed out', 'Changed', 'Inserted'], description='Layer type:', disabled=False, button_style='')

# GUI layout
button_goto = widgets.Button(description='Go to')
button_layout = widgets.HBox((button_previous, button_next))
navi_layout = widgets.VBox((layer_type, button_layout))
all_items = widgets.VBox((navi_layout, out1))
# Layout settings for the GUI
all_items.layout.align_items = 'center'
button_layout.layout.align_items = 'center'
navi_layout.layout.align_items = 'center'

# finish GUI
# connect buttons and functionality
layer_type.observe(on_trait_changed_layers)
button_next.on_click(on_button_clicked_next)
button_previous.on_click(on_button_clicked_previous)

# start GUI
init_gui()
display(all_items)
