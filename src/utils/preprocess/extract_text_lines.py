#   extract_text_lines(img, img_gray, img_bin, smooth, s, sigma, off, peak_dist):
#
#  function that runs our proposed method for text line extraction using 
#  projection profile matching and constrained seam carving.
#
#  Input:
#      img: original image
#      smooth: smoothing parameter for projection profile matching
#      sigma: standard deviation of the gaussian filter for energy map computation
#      s: number of image slices for projection profile matching
#      off: parameter that corresponds to seam thickness
#      peak_dist: minimal distance between two peaks from detected textlines
#
#  Output:
#      img_medial_seams: original image overlaid with medial seams
#      sep_seams: 2-D matrix where each column contains the coordinates of a separating seam
#      img_sep_seams: original image overlaid with separating seams
#
#  Parameter tuning:
#    The two main parameters of the function are "smooth" and "s". Both of them are crucial to 
#    the accuracy of the algorithm. 
#      - The smoothing is important, because the projection profile 
#      is very ragged in any real manuscript page, and the amount of smoothing influences the number 
#      of detected lines. Usually, a very small value is needed in the range [0.01, 0.0001].
#      - The number of slices is also important and it depends on the image resolution and layout. 
#      Usually, a slice number of 4 works well in practice. For the dataset of Saabni etal. we use s = 4.
#      For the manuscript of Aline we use s = 8, since the resolution is much higher.
#    In general, there is no automatic way to select these parameters.  
#      - "sigma" is not crucial and small values (<3) usually work well.
#      - small values of "off" are good for visualization purposes (range [1,5]).

from scipy import ndimage, misc
import utils.preprocess.medial_seam_func as cms
import utils.preprocess.seperating_seam_func as css
import numpy as np
import cv2


def extract_text_lines(img, img_gray, img_bin, smooth, s, sigma, off, peak_dist):
    # medial seam computation with projection profile matching
    med_seams = cms.compute_medial_seams(img_bin_masked=np.copy(img_bin), smooth=smooth, slice_num=s, peak_dist=peak_dist)
    sep_seams = css.compute_separating_seams(np.copy(img), img_bin, med_seams, off)
    return med_seams, sep_seams
