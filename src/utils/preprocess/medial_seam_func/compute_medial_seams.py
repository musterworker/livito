#  compute_medial_seams(img, img_bin_masked, smooth, slice_num, peak_dist)
#  return local_max, medial_seams_inds, L
#
#  function that is based on projection profile matching for medial seam computation.
#  The image is split into slices and local maxima of the edge image are matched 
#  between two consecutive slices. After the process of all slices, we obtain 
#  piece-wise linear approximation of text lines.
#  The output is a matrix where each element (i,j) contains the column 
#  coordinate of the i-th seam. j ranges from 1 to the page width.
#
#  Input:
#      img: original image
#      img_bin_masked: binary image which needs to be masked
#      smooth: smoothing parameter for projection profile matching
#      slice_num: number of image slices for projection profile matching
#      off: parameter that corresponds to seam thickness
#      peak_dist: minimal distance between two peaks of detected textlines
#
#  Output:
#      local_max: local maxima of the projection profiles of each slice
#      medial_seams_inds: linear indices of the text line approximations
#      L: cell array where each cell element contains the row coordinates of the medial seams
#      img_medial_seams: original image overlaid with the medial seams
#
#  Parameter tuning:
#    The two main parameters of the function are "smooth" and "s". Both of them are crucial to 
#    the accuracy of the algorithm. 
#      - The smoothing is important, because the projection profile 
#      is very ragged in any real manuscript page, and the amount of smoothing influences the number 
#      of detected lines. Usually, a very small value is needed in the range [0.01, 0.0001].
#      - The number of slices is also important and it depends on the image resolution and layout. 
#      Usually, a slice number of 4 works well in practice. For the dataset of Saabni etal. we use s = 4.
#      For the manuscript of Aline we use s = 8, since the resolution is much higher.
#    In general, there is no automatic way to select these parameters.   
#      - small values of "off" are good for visualization purposes (range [1,5]).

from scipy.interpolate import splrep, splev
import numpy as np
import cv2
import peakutils
import itertools
from skimage import measure


# masking long vertical and horizontal lines and remove them from
# this way these lines will not get detected as textlines
def compute_masked_img(img_bin):
    img_bin = cv2.bitwise_not(img_bin)
    th2 = cv2.adaptiveThreshold(img_bin, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 15, -2)
    # working copies for horizontal and vertical lines
    horizontal = th2
    vertical = th2
    # structures to mask
    vertical_structure = cv2.getStructuringElement(cv2.MORPH_RECT, (1, 200))
    horizontal_structure = cv2.getStructuringElement(cv2.MORPH_RECT, (200, 1))
    # initial eroding process to minimize noise
    horizontal = cv2.erode(horizontal, horizontal_structure)
    vertical = cv2.erode(vertical, vertical_structure)
    # expand masked regions
    for i in range(7):
        horizontal_structure = cv2.getStructuringElement(cv2.MORPH_RECT, (50, 1 + i))
        horizontal = cv2.dilate(horizontal, horizontal_structure)

        vertical_structure = cv2.getStructuringElement(cv2.MORPH_RECT, (1 + i, 50))
        vertical = cv2.dilate(vertical, vertical_structure)
    # inverting masks
    horizontal_inv = cv2.bitwise_not(horizontal)
    vertical_inv = cv2.bitwise_not(vertical)
    # masking image horizontally and vertically
    masked_img = cv2.bitwise_and(th2, th2, mask=horizontal_inv)
    masked_img = cv2.bitwise_and(masked_img, masked_img, mask=vertical_inv)
    # reverse the image to original color coding
    masked_img_inv = cv2.bitwise_not(masked_img)
    return masked_img_inv

# convert subindices to linear indices
def sub2ind(array_shape, rows, cols):
    cols = np.transpose(cols)
    if int(len(rows[0])) == len(cols):
        results = rows * array_shape[1] + cols
    else:
        add_const = cols[0]
        cols = [i + add_const for i in range(0, len(rows[0]))]
        results = rows * array_shape[1] + cols
    results = results.astype(int)
    return results[0]

# convert linear indeces to subindces
def ind2sub(array_shape, ind):
    ind = np.asarray(ind, dtype="int32")
    ind[ind < 0] = -1
    ind[ind >= array_shape[0] * array_shape[1]] = -1
    cols = ind % array_shape[1]
    rows = (ind.astype(int) / array_shape[1])
    return rows, cols


# intersection function as implemented in Matlab
def intersect_mtlb(a, b):
    a1, ia = np.unique(a, return_index=True)
    b1, ib = np.unique(b, return_index=True)
    aux = np.concatenate((a1, b1))
    aux.sort()
    c = aux[:-1][aux[1:] == aux[:-1]]
    return c, ia[np.in1d(a1, c)], ib[np.in1d(b1, c)]


def compute_medial_seams(img_bin_masked, smooth, slice_num, peak_dist):
    img_bin_masked = compute_masked_img(img_bin_masked)
    img_edge = cv2.Canny(img_bin_masked, 140, 170)
    img_edge[img_edge == 255] = 1
    # parameter initialization 
    height, width = img_edge.shape
    height = int(height)
    width = int(width)
    # binary image that will contain the piece-wise text line approximations
    img_bin = np.zeros((height * width, 1))
    # width of each image slice
    w = int(width / slice_num)
    # compute horizontal projection profiles of all edge image slices 
    # and find their local maxima
    local_max = list()  # locations of local maxima
    k = 0  # help counter for the slices
    horizontal_projections = list()
    spl_list = list()
    for i in range(0, slice_num):  # for each slice
        # compute histogram of sum of edges
        horiz_proj = sum(np.transpose(img_edge[:, k:k + w - 1]))
        horiz_proj = horiz_proj.astype(dtype="int32")
        horizontal_projections.append(horiz_proj)
        # smooth it with cubic splines
        horiz_proj_smooth_splrep = splrep(np.array(range(int(height))), horiz_proj, s=smooth, k=4)
        xs = np.array(range(int(height)))
        spl_eval = splev(xs, horiz_proj_smooth_splrep)
        spl_list.append(spl_eval)
        # find peaks of the profile
        local_max.append(peakutils.indexes(horiz_proj, thres=0.2, min_dist=peak_dist))
        # next slice
        k += w

    # half of slice width <-!!
    k = int(w / 2)
    medial_seams_inds = list()

    # match local maxima of the projection profiles 
    # between two consecutive image slices
    for i in range(slice_num - 1):
        # if there is no local max, continue to the next pair
        if len(local_max[i]) == 0:
            continue
        # matches from left to right and from right to left
        matches_lr = np.zeros((len(local_max[i]), 1), dtype="int32")
        matches_rl = np.zeros((len(local_max[i + 1]), 1), dtype="int32")
        # find matches from left slice to right slice
        for j in range(len(local_max[i])):
            # for each left local maximum
            # compute distances from the right slice and sort them
            dists = abs(local_max[i][j] - local_max[i + 1])
            ind_sort = np.argsort(dists)
            # the match is the right maximum that is closest to the left one
            matches_lr[j] = ind_sort[0]
        # find matches from right slice to left slice
        for j in range(len(local_max[i + 1])):
            # for each right local maximum
            # compute distances from the left slice and sort them
            dists1 = abs(local_max[i] - local_max[i + 1][j])
            ind_sort1 = np.argsort(dists1)
            # the match is the left maximum that is closest to the right one
            matches_rl[j] = ind_sort1[0]
        # match profile maxima that agree from both sides
        # (left to right and right to left)
        for j in range(len(local_max[i])):
            for p in range(len(local_max[i + 1])):
                # maximum match exists
                if matches_lr[j] == p and matches_rl[p] == j:
                    # calculate rounded row coordinates of lines between matched maxima
                    if i == 0:
                        # start the first histogram profile from the beginning of the image
                        # rounded row coordinates for the first pair of slices
                        points = np.array(list(map(round, np.linspace(local_max[i][j], local_max[i + 1][p], num=k))), ndmin=2,
                                          dtype="int32")
                        # linear indices on the entire image
                        inds = sub2ind([height, width], points, range(0, k))
                        medial_seams_inds.append(inds)
                    elif i == slice_num - 2:
                        # end the last histogram profile in the end of the image
                        length = len(range(k, width))
                        # rounded row coordinates for the last pair of slices
                        points = np.array(list(map(round, np.linspace(local_max[i][j], local_max[i + 1][p], num=length))),
                                          ndmin=2, dtype="int32")
                        # linear indices on the entire image
                        inds = sub2ind([height, width], points, range(k, width))
                        medial_seams_inds.append(inds)
                    else:
                        # intermediate pairs of slices
                        # rounded row coordinates for an intermediate pair of slices
                        points = np.array(list(map(round, np.linspace(local_max[i][j], local_max[i + 1][p], num=w))), ndmin=2,
                                          dtype="int32")
                        # linear indices on the entire image
                        inds = sub2ind([height, width], points, range(k, k + w - 1))
                        medial_seams_inds.append(inds)
                    # update the binary image with the text line approximation
                    for whitePixel in inds:
                        img_bin[whitePixel] = 255  # for visualisation and 1 for calculation
        # update the help counter 
        if 0 < i < slice_num - 1:
            k += w

    # reshape the binary image of medial seams
    img_bin = img_bin.reshape((height, width))
    # unique linear indices
    medial_seams_inds = list(itertools.chain.from_iterable(medial_seams_inds))
    medial_seams_inds = set(medial_seams_inds)
    medial_seams_inds = list(medial_seams_inds)

    # post-processing step to remove lines that start from some intermediate 
    # column of the image
    # connected component analysis of the binary image containing the 
    # piece-wise medial seams number of connected components
    connected_components, num_cc = measure.label(img_bin, neighbors=8, background=0, connectivity=2, return_num=True)
    region_properties = measure.regionprops(connected_components)
    # remove medial seams that are too small from the image
    for c in range(num_cc):
        indexes_cc = region_properties[c].coords
        # discard small components, here threshold!!!!!
        if len(indexes_cc) < int(width / 3):
            for i in indexes_cc:
                # update the binary image
                img_bin[i[0]][i[1]] = 0
    # remove the small lines from medial seams
    img_bin_lin = img_bin.reshape((height * width, 1))
    index = np.argwhere(img_bin_lin != 0)
    index = index[:, 0]
    medial_seams_inds = index.tolist()

    # post-processing step to extend the small lines towards the end 
    # column of the image
    # connected component analysis of the binary image containing the 
    # piece-wise medial seams
    connected_components, num_cc = measure.label(img_bin, neighbors=8, background=0, connectivity=2, return_num=True)
    region_prop_list = measure.regionprops(connected_components)
    # extend medial seams to the end of the image, if possible
    for c in range(num_cc-1):
        # column coordinates of the c-th connected component
        indexes_cc = region_prop_list[c].coords
        # sort numpy array, by J which leads from 0 to end
        temp = indexes_cc.view(np.ndarray)
        indexes_cc = temp[np.lexsort((temp[:, 1],))]
        I = indexes_cc[:, 0]
        J = indexes_cc[:, 1]
        # extract the lower (previous) CC
        prev_cc = region_prop_list[c - 1].coords
        I_p = prev_cc[:, 0]
        # extract the upper(next) CC
        next_cc = region_prop_list[c + 1].coords
        I_n = next_cc[:, 0]
        # check if this connected component starts in the first column of the
        # image, if not, extend it towards the beginning
        if J[0] != 0:
            start_col = J[0]
            if c == 0:
                # create linear indices between the first row and the row
                # coordinates of the connected component
                inds_ext = np.array(list(map(round, np.linspace(0, I[0], num=start_col))), ndmin=2, dtype="int32")
                inds_ext = sub2ind([height, width], inds_ext, range(0, start_col))
            elif c == num_cc-1:
                # create linear indices between the last row and the row
                # coordinates of the connected component
                inds_ext = np.array(list(map(round, np.linspace(height - 2, I[0], num=start_col))), ndmin=2, dtype="int32")
                inds_ext = sub2ind([height, width], inds_ext, range(0, start_col))
            else:
                start_col = J[0]
                # of the upper and lower CC's until an intersection is found
                # middle end point
                middle = round((I_p[-1] + I_n[-1]) / 2)
                inds_ext = np.array(list(map(round, np.linspace(middle, I[0], num=start_col))), ndmin=2, dtype="int32")
                inds_ext = sub2ind([height, width], inds_ext, range(0, start_col))
            # add the extended indices to the full indices array
            inds_ext = inds_ext.tolist()
            medial_seams_inds = medial_seams_inds + inds_ext

        # check if this connected component ends in the last column of the
        # image, if not, extend it towards the end
        if J[-1] != width-1:
            # last column of the CC
            end_col = J[-1]
            end_col_ind = len(J)-1
            # the top connected component
            if c == 0:
                # create linear indices between the first row and the row
                # coordinates of the connected component
                inds_ext = np.array(list(map(round, np.linspace(I[end_col_ind], 0, num=width - end_col + 1))), ndmin=2, dtype="int32")
                inds_ext = sub2ind([height, width], inds_ext, range(end_col, width))
                # add the extended indices to the full indices array
                inds_ext = inds_ext.tolist()
                medial_seams_inds = medial_seams_inds + inds_ext
            # the bottom connected component
            elif c == num_cc-1:
                # create linear indices between the last row and the row
                # coordinates of the connected component
                inds_ext = np.array(list(map(round, np.linspace(I[end_col_ind], height - 2, num=width - end_col + 1))), ndmin=2, dtype="int32")
                inds_ext = sub2ind([height, width], inds_ext, range(end_col, width))
                # add the extended indices to the full indices array
                inds_ext = inds_ext.tolist()
                medial_seams_inds = medial_seams_inds + inds_ext
            # intermediate connected component
            else:
                # of the upper and lower CC's until an intersection is found
                # middle end point
                middle = round((I_p[0]+I_n[0])/2)
                # extend towards the end
                inds_ext = np.array(list(map(round, np.linspace(I[end_col_ind], middle, num=width - end_col))), ndmin=2, dtype="int32")
                # find intersections of the extended CC to the lower and upper ones
                inter_p, ip1, ip2 = intersect_mtlb(inds_ext, I_p)
                inter_n, in1, in2 = intersect_mtlb(inds_ext, I_n)
                if len(inter_p) > 0 and len(inter_n) > 0:  # if both intersections
                    pass
                elif len(inter_p) > 0:
                    # intersection with the upper CC
                    # extend the approximation until the first intersection
                    t = abs(ip2[0] - end_col)
                    inds_ext = inds_ext[0:t-10]
                    inds_ext = sub2ind((height, width), inds_ext, range(end_col, end_col + t - 1 - 10))
                elif len(inter_n) > 0:  # intersection with the lower CC
                    # extend the approximation until the first intersection
                    t = abs(in2[0] - end_col)
                    inds_ext = inds_ext[0:t-10]
                    inds_ext = sub2ind((height, width), inds_ext, range(end_col, end_col + t - 1 - 10))
                else:    # if no intersection, just convert them to linear indices
                    inds_ext = sub2ind([height, width], inds_ext, range(end_col, width))
            #     # add the extended indices to the full indices array
                inds_ext = inds_ext.tolist()
                medial_seams_inds = medial_seams_inds + inds_ext

    # binary image with only the medial seams
    img_bin = np.zeros((height * width, 1))
    img_bin[medial_seams_inds] = 255
    img_bin = img_bin.reshape((height, width))

    # connected component analysis of the final binary image containing 
    # the medial seams
    connected_components, num_cc = measure.label(img_bin, neighbors=8, background=0, connectivity=2, return_num=True)
    region_properties = measure.regionprops(connected_components)
    L = list()
    # for each connected component
    for c in range(num_cc):
        indexes_cc = region_properties[c].coords
        # discard small components, here threshold and components larger than image width
        if indexes_cc.shape[0] > width or indexes_cc.shape[0] < int(width / 2):
            continue
        # sorting array from left to right, as coords output does not give sorted output
        ind = np.argsort(indexes_cc[:, 1])
        indexes_cc = indexes_cc[ind]
        # append transposed lines
        indexes_cc = np.transpose(indexes_cc)
        rows = indexes_cc[0][:]
        L.append(rows)

    return L
