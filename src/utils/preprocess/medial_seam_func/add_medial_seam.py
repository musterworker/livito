# add_medial_seam(points, image_width, Lines)
# return Lines
# Input:
# Output:
# points containing the support coordinates of the new textline
# coordintes need to be expand to image width and added to lines as well as
# medial seams in linear form

import numpy as np


def add_medial_seam(points, image_width, Lines):
    # truncate string into list of numerical points
    points = points.replace("(", "")
    points = points[:-1].split(")")
    numerical_points = list()
    for i in points:
        values = i.split(",")
        # adjust scaled value
        numerical_points.append((int(int(values[0]) / 0.3), int(int(values[1]) / 0.3)))
    # sort by smallest column first
    numerical_points.sort()
    end = image_width
    # extending the points to the beginning and end of the image
    for i in range(len(numerical_points)):
        # if points do not start at column 0
        if i == 0 and numerical_points[i][0] != 0:
            linear_space = np.linspace(start=numerical_points[i][1], stop=numerical_points[i][1],
                                       num=numerical_points[i][0])
            result = linear_space
            linear_space = np.linspace(start=numerical_points[i][1], stop=numerical_points[i + 1][1],
                                       num=numerical_points[i + 1][0] - numerical_points[i][0])
            result = np.append(result, linear_space)
        # if last point does end before column at image end
        elif i == len(numerical_points) - 1 and numerical_points[len(numerical_points) - 1][0] < end:
            linear_space = np.linspace(start=numerical_points[i][1], stop=numerical_points[i][1],
                                       num=end - numerical_points[i][0])
        # if last col exceeds image, take previous point for linear extension
        elif i == len(numerical_points) - 1 and numerical_points[len(numerical_points) - 1][0] > end:
            linear_space = np.linspace(start=numerical_points[i - 1][1], stop=numerical_points[i - 1][1],
                                       num=end - numerical_points[i - 1][0])
        else:
            linear_space = np.linspace(start=numerical_points[i][1], stop=numerical_points[i + 1][1],
                                       num=numerical_points[i + 1][0] - numerical_points[i][0])
            # append linear spaces
        if i > 0:
            result = np.append(result, linear_space)
        elif i == 0 and numerical_points[i][0] == 0:
            result = linear_space
    result = result.astype(dtype="int32")

    # add to  Lines
    Lines.append(result)
    # sort Lines from smallest
    Lines.sort(key=lambda x: x[0])

    return Lines
