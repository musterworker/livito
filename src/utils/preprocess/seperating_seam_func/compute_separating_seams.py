#  compute_separating_seams(img, img_bin, L, off):
#
#  function that computes horizontal minimum energy separating seams using constrained
#  seam carving. The input to the algorithm is the coordinates of the medial seams
#  found in the previous text using the function match_hists.
#  The seam computation is constrained before two consecutive medial seams.
#  The procedure is repeated until all pairs of text lines are processed.
#
#  Input:
#      img: original color image
#      img_gray: original grayscale image
#      L: medial seams
#      sigma: standard deviation of the gaussian filter for energy map computation
#      off: parameter that corresponds to seam thickness
#
#  Output:
#      sep_seams: coordinates of computed separating seams
#      img_sep_seams: original color image overlaid with the computed separating seams
#
#  Parameter tuning:
#    The two main parameters of the function is "sigma" and "off".
#      - "sigma" is used for prior blurring
#      before computing the gradient energy map. It depends on the image resolution. However,
#      it is not a crucial parameter, and small values (<3) work in general well.
#      - "off" corresponds to seam thickness when the separating seams are presented in a figure.

import numpy as np
from skimage import measure
from skimage.morphology import binary_dilation
from operator import itemgetter
from itertools import groupby


def move_out_of_black_pixel_area(img, row, col, limit, direction):
    while img[row, col] == 0 or row == limit:
        if direction == "down":
            row += 1
        elif direction == "up":
            row -= 1
    return row


def make_deluted_working_copy(img_bin):
    img_bin_reversed = np.copy(img_bin)
    img_bin_reversed[img_bin_reversed == 0] = 1
    img_bin_reversed[img_bin_reversed == 255] = 0
    # delute twice
    img_bin_deluted = binary_dilation(img_bin_reversed)
    img_bin_deluted = binary_dilation(img_bin_deluted)
    img_bin_deluted = binary_dilation(img_bin_deluted)
    img_bin_deluted.dtype = 'uint8'
    img_bin_deluted[img_bin_deluted == 0] = 255
    img_bin_deluted[img_bin_deluted == 1] = 0
    return img_bin_deluted


def compute_separating_seams(img, img_bin, L, off):
    # delute binary image
    img_bin_deluted = make_deluted_working_copy(img_bin)
    height, width, _ = img.shape
    textline_num = len(L)
    # initialize the seam image
    img_sep_seams = np.copy(img)
    # initialize seam coordinates
    sep_seams = np.zeros((width, textline_num - 1))
    all_black_pixels = list()
    mean_seam_border_delta = 5
    hit_black_pixel_left = False
    hit_black_pixel_right = False
    additional_gap = 2
    # apply constrained seam carving for each pair of text lines
    for i in range(textline_num - 1):
        # upper and lower medial seams
        upper_medial_seam = L[i]
        lower_medial_seam = L[i + 1]
        black_pixel_positions = list()
        # length of the medial seam (maybe they do not extend
        # through the whole page width)
        length_upper_medial_seam = len(upper_medial_seam)
        length_lower_medial_seam = len(lower_medial_seam)
        min_length = min(length_upper_medial_seam, length_lower_medial_seam) - 1  # minimum of the two text line lengths
        for col in range(min_length + 1):
            # get mean distance from current medial seams
            medium_val = int((upper_medial_seam[col] + lower_medial_seam[col]) / 2)
            # medium_dist = medium_val - upper_medial_seam[col]
            # if next pixel is not black and current pixel is white
            if img_bin_deluted[medium_val][col] == 255:
                sep_seams[col, i] = medium_val
            elif img_bin_deluted[medium_val][col] == 0:
                black_pixel_positions.append(col)
        # rearrange black pixels to groups of consecutive pixels
        grouped_blackpixels = list()
        for k, g in groupby(enumerate(black_pixel_positions), lambda i_x: i_x[0] - i_x[1]):
            group = list(map(itemgetter(1), g))
            grouped_blackpixels.append((group[0], group[-1]))
        all_black_pixels.append(grouped_blackpixels)

    # move seam position for all groups
    # moving directions are -1 for up, 1 for down,0 for added at mean_dist and  2 for not moved
    moved_groups = list()
    moved_directions = list()
    for idx, groups in enumerate(all_black_pixels):
        if len(groups) == 0:
            continue
        moved_group = list()
        moved_direction = list()
        for group in groups:
            group_col_start = group[0]
            group_col_end = group[-1]
            delta = 60
            # prevent search window to run out of image range
            if group_col_start - delta >= 0:
                search_window_left_border = group_col_start - delta
            else:
                search_window_left_border = 0
            if group_col_end + delta <= width - 1:
                search_window_right_border = group_col_end + delta
            else:
                search_window_right_border = width - 1
            # get the connected component
            search_window = img_bin_deluted[L[idx][group_col_start]: L[idx + 1][group_col_start],
                            search_window_left_border:search_window_right_border]
            connected_components = measure.label(search_window, neighbors=8, background=255, connectivity=1)
            cc_height, cc_width = connected_components.shape
            # get label for the black pixel component from group
            label = connected_components[int(cc_height / 2)][int(cc_width / 2)] - 1
            region_properties = measure.regionprops(connected_components)
            region_bbox = region_properties[label].bbox
            # get label left and right borders
            label_border_left = region_bbox[1]
            label_border_right = region_bbox[3]
            # get centroid
            region_centroid = region_properties[label].centroid
            # the connected component is growing from upper to mean seam
            if region_bbox[0] < mean_seam_border_delta and region_bbox[2] < cc_height - mean_seam_border_delta:
                # move groups
                new_val = L[idx][group_col_start] + region_bbox[2] + additional_gap
                # expand group range and seperating seam until first black pixel found
                while hit_black_pixel_left != True and hit_black_pixel_right != True:
                    # check left range
                    if hit_black_pixel_left != True:
                        i = 0
                        for i in range(-1, label_border_left - delta - 1, -1):
                            if img_bin_deluted[new_val][group_col_start + i] == 0 or group_col_start + i == 0:
                                hit_black_pixel_left = True
                                break
                        hit_black_pixel_left = True
                    # check left range
                    if hit_black_pixel_right != True:
                        j = 0
                        for j in range(label_border_right - delta - 1):
                            if img_bin_deluted[new_val][group_col_end + j] == 0 or group_col_end + j == min_length:
                                hit_black_pixel_right = True
                                break
                        hit_black_pixel_right = True
                # set back values to default
                hit_black_pixel_left = False
                hit_black_pixel_right = False
                # save seams and new group range
                sep_seams[range(group_col_start + i, group_col_end + j), idx] = new_val
                group = (group_col_start + i, group_col_end + j)
                # memorize moved groups and there directions
                moved_group.append(group)
                moved_direction.append(1)
            # the connected component is growing from lower to mean seam
            elif region_bbox[2] > cc_height - mean_seam_border_delta and region_bbox[0] > mean_seam_border_delta:
                # move groups
                new_val = L[idx][group_col_start] + region_bbox[0] - additional_gap
                # expand group range and seperating seam until first black pixel found
                while hit_black_pixel_left != True and hit_black_pixel_right != True:
                    # check left range
                    if hit_black_pixel_left != True:
                        i = 0
                        for i in range(-1, label_border_left - delta - 1, -1):
                            if img_bin_deluted[new_val][group_col_start + i] == 0 or group_col_start + i == 0:
                                hit_black_pixel_left = True
                                break
                        hit_black_pixel_left = True
                    # check left range
                    if hit_black_pixel_right != True:
                        j = 0
                        for j in range(label_border_right - delta - 1):
                            try:
                                if img_bin_deluted[new_val][group_col_end + j] == 0 or group_col_end + j == min_length:
                                    hit_black_pixel_right = True
                                    break
                            except IndexError:
                                break
                        hit_black_pixel_right = True
                # set back values to default
                hit_black_pixel_left = False
                hit_black_pixel_right = False
                # save seams and new group range
                sep_seams[range(group_col_start + i, group_col_end + j), idx] = new_val
                group = (group_col_start + i, group_col_end + j)
                # memorize moved groups and there directions
                moved_group.append(group)
                moved_direction.append(-1)
            # for continous black areas from upper to lower mean take medium val from group range borders
            elif region_bbox[0] == 0 and region_bbox[2] == cc_height:
                # memorize moved groups and there directions
                moved_group.append(group)
                moved_direction.append(0)
                # move groups
                new_val = (L[idx][group_col_start] + L[idx + 1][group_col_end]) / 2
                sep_seams[range(group_col_start, group_col_end + 1), idx] = new_val
            else:  # labeled region in the center of the seperating seam, far from borders
                # get centroid for labels above mean dist
                if int(region_centroid[0]) <= cc_height / 2 and region_bbox[0] > 15 and region_bbox[2] < cc_height - 15:
                    # move groups
                    new_val = L[idx][group_col_start] + region_bbox[2] + additional_gap
                    # expand group range and seperating seam until first black pixel found
                    while hit_black_pixel_left != True and hit_black_pixel_right != True:
                        # check left range
                        if hit_black_pixel_left != True:
                            i = 0
                            for i in range(-1, label_border_left - delta - 1, -1):
                                if img_bin_deluted[new_val][group_col_start + i] == 0 or group_col_start + i == 0:
                                    hit_black_pixel_left = True
                                    break
                            hit_black_pixel_left = True
                        # check left range
                        if hit_black_pixel_right != True:
                            j = 0
                            for j in range(label_border_right - delta - 1):
                                try:
                                    if img_bin_deluted[new_val][group_col_end + j] == 0 or group_col_end + j == min_length:
                                        hit_black_pixel_right = True
                                        break
                                except IndexError:
                                    break
                            hit_black_pixel_right = True
                    # set back values to default
                    hit_black_pixel_left = False
                    hit_black_pixel_right = False
                    # save seams and new group range
                    sep_seams[range(group_col_start + i, group_col_end + j), idx] = new_val
                    # memorize moved groups and there directions
                    moved_group.append(group)
                    moved_direction.append(-1)
                    # get centroid for labels below mean dist
                elif int(region_centroid[0]) > cc_height / 2 and region_bbox[0] > 15 and region_bbox[
                    2] < cc_height - 15:
                    # move groups
                    new_val = L[idx][group_col_start] + region_bbox[0] - additional_gap
                    # expand group range and seperating seam until first black pixel found
                    while hit_black_pixel_left != True and hit_black_pixel_right != True:
                        # check left range
                        if hit_black_pixel_left != True:
                            i = 0
                            for i in range(-1, label_border_left - delta - 1, -1):
                                if img_bin_deluted[new_val][group_col_start + i] == 0 or group_col_start + i == 0:
                                    hit_black_pixel_left = True
                                    break
                            hit_black_pixel_left = True
                        # check left range
                        if hit_black_pixel_right != True:
                            j = 0
                            for j in range(label_border_right - delta - 1):
                                if img_bin_deluted[new_val][group_col_end + j] == 0 or group_col_end + j == min_length:
                                    hit_black_pixel_right = True
                                    break
                            hit_black_pixel_right = True
                    # set back values to default
                    hit_black_pixel_left = False
                    hit_black_pixel_right = False
                    # save seams and new group range
                    sep_seams[range(group_col_start + i, group_col_end + j), idx] = new_val
                    # memorize moved groups and there directions
                    moved_group.append(group)
                    moved_direction.append(1)
        # append to current line
        moved_groups.append(moved_group)
        moved_directions.append(moved_direction)

    # fill zero values in sep seams with average of surrounding seams
    for idx in range(textline_num - 1):
        # find zeros in array
        zeros = np.where(sep_seams[:, idx] == 0)
        zeros = zeros[0].tolist()
        # group consecutive zeros
        grouped_zeros = list()
        for k, g in groupby(enumerate(zeros), lambda i_x: i_x[0] - i_x[1]):
            group = list(map(itemgetter(1), g))
            grouped_zeros.append((group[0], group[-1]))
        # fill in mean values from neighbouring pixels
        if len(grouped_zeros) > 0:
            for group in grouped_zeros:
                if group[0] != group[1]:
                    try:
                        new_val = int((sep_seams[group[0] - 1, idx] + sep_seams[group[-1] + 1, idx]) / 2)
                        sep_seams[range(group[0] - 1, group[-1] + 1), idx] = new_val
                    except IndexError:
                        new_val = int((sep_seams[group[0] - 1, idx] + sep_seams[group[-1], idx]) / 2)
                        sep_seams[range(group[0] - 1, group[-1] + 1), idx] = new_val
                elif group[0] == group[1]:
                    sep_seams[group[0], idx] = sep_seams[group[0] - 1, idx]

    return sep_seams
