import tensorflow as tf
tf.logging.set_verbosity(tf.logging.ERROR)

from keras.models import Model, model_from_json
from PIL import Image
import random
import os
import cv2
import glob
from pathlib import Path
import pandas as pd
import numpy as np
from sklearn.manifold import TSNE

import plotly.graph_objects as go
import ipywidgets as widgets
from IPython.display import display

import colorlover as cl
from collections import Counter, OrderedDict
from tqdm import tqdm_notebook

import warnings
warnings.filterwarnings("ignore")


def genImgCrops(image_file, crop_size, relative_num_of_crops=0.6):
    images = []

    im = Image.open(image_file)
    cur_width = im.size[0]
    cur_height = im.size[1]

    # print(cur_width, cur_height)
    height_fac = crop_size / cur_height

    new_width = int(cur_width * height_fac)
    size = new_width, crop_size
    
    im = im.convert("L")
    imresize = im.resize((size), Image.ANTIALIAS)  # Resize so height = 113 while keeping aspect ratio
    new_width = imresize.size[0]

    # Generate crops of size 113x113 from this resized image and keep random 10% of crops
    avail_x_points = list(range(0, new_width - crop_size))# total x start points are from 0 to width -113

    # Pick random x%
    num_of_crops = int(len(avail_x_points)*relative_num_of_crops)
    # Now pick
    random_startx = random.sample(avail_x_points,  num_of_crops )

    for start in random_startx:
        imcrop = imresize.crop((start, 0, start+crop_size, crop_size))
        images.append(np.asarray(imcrop))

    # trim image to only see section with road
    X_train = np.array(images)
    
    #reshape X_train for feeding in later
    X_train = X_train.reshape(X_train.shape[0], crop_size, crop_size, 1)
    #convert to float and normalize
    X_train = X_train.astype('float32')
    X_train /= 255

    return X_train


def resize_image(image):
    import tensorflow as tf
    return tf.image.resize_images(image,[56,56])


def splitData(data, interval):
    splittedData = list()
    for i, length in enumerate(interval):
        x = np.ndarray.flatten(data[sum(interval[0:i]):sum(interval[0:i]) + length, 0:1])
        y = np.ndarray.flatten(data[sum(interval[0:i]):sum(interval[0:i]) + length, 1:2])
        z = np.ndarray.flatten(data[sum(interval[0:i]):sum(interval[0:i]) + length, 2:3])
        splittedData.append([x, y, z])

    return splittedData


def centeroidnp(arr, interval):
    centroids = list()
    #TODO: remove extrem values for better centroids
    for i, length in enumerate(interval):
        sum_x = np.sum(arr[sum(interval[0:i]):sum(interval[0:i])+length, 0])
        sum_y = np.sum(arr[sum(interval[0:i]):sum(interval[0:i])+length, 1])
        sum_z = np.sum(arr[sum(interval[0:i]):sum(interval[0:i])+length, 2])
        centroids.append([sum_x/length, sum_y/length, sum_z/length])
        
    centroids = np.array(centroids)
    return centroids



# on click on data point, the appropriate textline will be printed
def update_point(trace, points, selector):
    if len(points.point_inds) != 0 and points.trace_name != 'Centroiden':
        img = cv2.imread(sortedData[sum(yIntervals[:points.trace_index]) + points.point_inds[0], 1])
        img_str = cv2.imencode('.png', img)[1].tostring()
        with out_textlines:
            # print file name
            print(sortedData[sum(yIntervals[:points.trace_index]) + points.point_inds[0], 1])
            display(widgets.Image(value=img_str, format='png', width=900, height=600))


def build_model(model_name='checkpoint.hdf5'):
    json_file = open(Path.cwd().parent / "data" / 'scribe_detector_checkpoints' /"model.json", "r") #/ 'learned on original data crops' 
    loaded_model_json = json_file.read()
    json_file.close()

    model = model_from_json(loaded_model_json)
    model.load_weights(Path.cwd().parent / "data" / 'scribe_detector_checkpoints' / model_name, by_name=True) #/ 'learned on original data crops'
    num_classes = model.output_shape[1]
    return model, num_classes


# start application	
model, num_classes = build_model(model_name='checkpoint.hdf5')
image_filename = glob.glob(os.path.join('..', 'data', 'preprocess_data', 'output', '*', 'lines', '*.tif'))
out_textlines = widgets.Output()
allmedians = list()
y = list()
weighted_predictions = list()
weighted_predictions_per_page = list()
winners = list()
dirs = list()
csv_frame = np.zeros((len(image_filename), num_classes))
# iterate over all images on page level
for dir1 in tqdm_notebook(sorted(glob.glob(os.path.join('..', 'data', 'preprocess_data', 'output', '*', 'lines')))):
    dirs.append(dir1[8:-6])
    weighted_predictions_lines = list()
    for dir2 in sorted(glob.glob(os.path.join(dir1, '*.tif'))):
        # load text line
        img_crops = genImgCrops(dir2, crop_size=model.input_shape[1], relative_num_of_crops=0.1)
        # get layer to retrieve features from model
        layer_model_dense2 = Model(inputs=model.input, outputs=model.get_layer('dense2').output)
        # predict features
        output_dense2 = layer_model_dense2.predict(img_crops)
        # predict scribe class
        c = Counter(model.predict_classes(img_crops))
        scribe_class = list(c.keys())
        pred = list(c.values())
        y.append(scribe_class[pred.index(max(pred))])
        # get relative confidence level of prediction
        d = dict()
        for j in range(len(scribe_class)):
            d[scribe_class[j]] = pred[j]/sum(pred)
        d = dict(OrderedDict(sorted(d.items())))
        weighted_predictions.append(d)
        weighted_predictions_lines.append(d)
        # using median to condense all feature crops to single array per image
        mean = np.mean(output_dense2, axis=0)
        median = np.median(output_dense2, axis=0)
        allmedians.append(median)
    
    weighted_predictions_per_page.append(weighted_predictions_lines)
    
# save results to csv on line level
for idx, d in enumerate(weighted_predictions):
    for key, value in d.items():
        csv_frame[idx][int(key)] = value
columns = ['Scribe_'+str(i) for i in range(1, num_classes+1)]
columns = ['File_Path'] + columns
new_csv_frame = np.hstack((np.array(image_filename, ndmin=2).T, csv_frame))
pd.DataFrame(new_csv_frame).to_csv('../save/scribe_detector_results_line_level.csv', sep=';', header=columns, index=False)

# save results to csv on page level
csv_frame = np.zeros((len(dirs), num_classes))
for i in range(len(weighted_predictions_per_page)):
    csv_frame_buffer = np.zeros((len(weighted_predictions_per_page[i]), num_classes))
    for idx, d in enumerate(weighted_predictions_per_page[i]):
        for key, value in d.items():
            csv_frame_buffer[idx][int(key)] = value
    csv_frame[i] = np.mean(csv_frame_buffer, axis=0)
columns = ['Scribe_'+str(i) for i in range(1, num_classes+1)]
columns = ['File_Path'] + columns
new_csv_frame = np.hstack((np.array(dirs, ndmin=2).T, csv_frame))
pd.DataFrame(new_csv_frame).to_csv('../save/scribe_detector_results_page_level.csv', sep=';', header=columns, index=False)

# create tsne plot
# as tsne is a stochastic algorithm. Results will not always look the same, but in average they do.
# splitted data neu sortieren, entsprechend der predictions!!!
allmedians = np.stack(allmedians)
y = np.array(y)+1

# prepare data for tsne visualisation
yCounter = Counter(y)
yDict = dict(yCounter)
global yIntervals
yIntervals = list(OrderedDict(sorted(yDict.items())).values())
target_names = list(OrderedDict(sorted(yDict.items())).keys())

# create sorted data stack after predictions for textlines
y = np.reshape(y, (y.shape[0], 1))
fn = np.array(image_filename)
fn = np.reshape(fn, (fn.shape[0], 1))
w = np.array(weighted_predictions)
w = np.reshape(w, (w.shape[0], 1))
sortedData = np.concatenate((y, fn, w, allmedians), axis=1)
sortedData = sortedData[sortedData[:,0].argsort()]

X_tsne = TSNE(learning_rate=50, n_components=3).fit_transform(sortedData[:, 3:])
splittedData = splitData(X_tsne, yIntervals)
centroids = centeroidnp(X_tsne, yIntervals)

# if num_classes > 2 and < 13 you can use colorScale, else remove color setting from scatter plots
colorScale = cl.scales[str(num_classes)]['qual']['Paired']
data = []

# add tsne traces
for idx, value in enumerate(splittedData):
    # get confidence level for each text line. Will be displayed on hovering.
    confLevels = ['Confidence Level:'+str(sortedData[i, 2][sortedData[i, 0]-1])[:4] for i in range(sum(yIntervals[:idx]), sum(yIntervals[:idx])+yIntervals[idx])]
    trace = go.Scatter3d(x=value[0], y=value[1], z=value[2], mode='markers',name=str(target_names[idx]), hovertext=confLevels, hoverinfo='text',
                         marker=dict(size=10, color=colorScale[idx], line=dict(color=colorScale[idx], width=0.5), opacity=0.8))
    data.append(trace)
# add centroids
centroidTrace = go.Scatter3d(x=np.ndarray.flatten(centroids[:, 0:1]), y=np.ndarray.flatten(centroids[:, 1:2]), z=np.ndarray.flatten(centroids[:, 2:3]),
    mode='markers', name="Centroiden", marker=dict(color='rgb(255, 255, 255)',  size=20, symbol='circle', line=dict(color='rgb(204, 204, 204)', width=1), opacity=0.5))
data.append(centroidTrace)

layout = go.Layout(margin=dict(l=0, r=0, b=0, t=0), legend=dict(x=-0.1, y=1), xaxis=dict(title='nope'))
fig = go.FigureWidget(data=data, layout=layout)
fig.update_layout(scene=dict(xaxis=dict(title='X'), yaxis=dict(title='Y'), zaxis=dict(title='Z')))

# add functionality to plot
for trace in fig.data:
    trace.on_click(update_point)

display(fig)
