from __future__ import division
import os
import pathlib as p
import random
import pickle
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
import cv2

from sklearn.preprocessing import LabelEncoder
from sklearn.utils import class_weight
from sklearn.model_selection import train_test_split
from sklearn.manifold import TSNE

from keras.models import Model
from keras.utils import to_categorical
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Lambda, Activation, BatchNormalization
from keras.layers.convolutional import Convolution2D, ZeroPadding2D, MaxPooling2D
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint, EarlyStopping

import plotly.graph_objects as go
import ipywidgets as widgets 
from IPython.display import display, clear_output

import colorlover as cl
from collections import Counter

import warnings
warnings.filterwarnings("ignore")

# set environment variable for GPU computing
os.environ["CUDA_VISIBLE_DEVICES"] = '1'

# Generator function for generating random crops from each sentence
# Now create generators for randomly cropping 113x113 patches from these images
# Start with train generator shared in the class and add image augmentations
def generate_data(samples, target_files, cropSize=113,  batch_size=16, factor = 0.1 ):
    num_samples = len(samples)
    from sklearn.utils import shuffle
    while 1:  # Loop forever so the generator never terminates
        for offset in range(0, num_samples, batch_size):
            batch_samples = samples[offset:offset+batch_size]
            batch_targets = target_files[offset:offset+batch_size]

            images = []
            targets = []
            for i in range(len(batch_samples)):
                batch_sample = batch_samples[i]
                batch_target = batch_targets[i]
                im = Image.open(batch_sample)
                cur_width = im.size[0]
                cur_height = im.size[1]
                height_fac = cropSize / cur_height

                new_width = int(cur_width * height_fac)
                size = new_width, cropSize
                
                im = im.convert("L")
                # Resize so height = 113 while keeping aspect ratio
                imresize = im.resize((size), Image.ANTIALIAS)  
                new_width = imresize.size[0]
                # Generate crops of size 113x113 from this resized image and keep random 10% of crops
                # total x start points are from 0 to width -113
                avail_x_points = list(range(0, new_width - cropSize ))
                # Pick random x%
                pick_num = int(len(avail_x_points)*factor)

                # Now pick
                random_startx = random.sample(avail_x_points,  pick_num)

                for start in random_startx:
                    imcrop = imresize.crop((start, 0, start+cropSize, cropSize))
                    images.append(np.asarray(imcrop))
                    targets.append(batch_target)

            # trim image to only see section with road
            X_train = np.array(images)
            y_train = np.array(targets)
            # reshape X_train for feeding in later
            X_train = X_train.reshape(X_train.shape[0], cropSize, cropSize, 1)
            # convert to float and normalize
            X_train = X_train.astype('float32')
            X_train /= 255
#            One hot encode y
            y_train = to_categorical(y_train, num_classes)

            yield shuffle(X_train, y_train)


def genImgCrops(image_file, crop_size, relative_num_of_crops=0.6):

    images = []

    im = Image.open(image_file)
    cur_width = im.size[0]
    cur_height = im.size[1]

    # print(cur_width, cur_height)
    height_fac = crop_size / cur_height

    new_width = int(cur_width * height_fac)
    size = new_width, crop_size
    
    im = im.convert("L")
    imresize = im.resize((size), Image.ANTIALIAS)  # Resize so height = 113 while keeping aspect ratio
    new_width = imresize.size[0]

    # Generate crops of size 113x113 from this resized image and keep random 10% of crops
    avail_x_points = list(range(0, new_width - crop_size))  # total x start points are from 0 to width -113

    # Pick random x%
    num_of_crops = int(len(avail_x_points)*relative_num_of_crops)
    # Now pick
    random_startx = random.sample(avail_x_points,  num_of_crops )

    for start in random_startx:
        imcrop = imresize.crop((start, 0, start+crop_size, crop_size))
        images.append(np.asarray(imcrop))

    # trim image to only see section with road
    X_train = np.array(images)
    
    # reshape X_train for feeding in later
    X_train = X_train.reshape(X_train.shape[0], crop_size, crop_size, 1)
    # convert to float and normalize
    X_train = X_train.astype('float32')
    X_train /= 255

    return X_train


def resize_image(image):
    import tensorflow as tf
    return tf.image.resize_images(image,[56,56])


def splitData(data, interval):
    splittedData = list()
    for i, length in enumerate(interval):
        x = np.ndarray.flatten(data[sum(interval[0:i]):sum(interval[0:i]) + length, 0:1])
        y = np.ndarray.flatten(data[sum(interval[0:i]):sum(interval[0:i]) + length, 1:2])
        z = np.ndarray.flatten(data[sum(interval[0:i]):sum(interval[0:i]) + length, 2:3])
        splittedData.append([x, y, z])

    return splittedData


def centeroidnp(arr, interval):
    centroids = list()
    # TODO: remove extrem values for better centroids
    for i, length in enumerate(interval):
        sum_x = np.sum(arr[sum(interval[0:i]):sum(interval[0:i])+length, 0])
        sum_y = np.sum(arr[sum(interval[0:i]):sum(interval[0:i])+length, 1])
        sum_z = np.sum(arr[sum(interval[0:i]):sum(interval[0:i])+length, 2])
        centroids.append([sum_x/length, sum_y/length, sum_z/length])
        
    centroids = np.array(centroids)
    return centroids


def show_training_results():
    plt.figure(figsize=(20,10))
    plt.plot(np.arange(0, len(history_object.epoch)), history_object.history["loss"], label="train_loss")
    plt.plot(np.arange(0, len(history_object.epoch)), history_object.history["val_loss"], label="val_loss")
    plt.plot(np.arange(0, len(history_object.epoch)), history_object.history["acc"], label="train_acc")
    plt.plot(np.arange(0, len(history_object.epoch)), history_object.history["val_acc"], label="val_acc")
    plt.title("Training Loss and Accuracy on Dataset")
    plt.xlabel("Epoch #")
    plt.ylabel("Loss/Accuracy")
    plt.legend(loc="upper left")
    plt.show()
    
    scores = model.evaluate_generator(test_generator,10) 
    print('Accuracy on test dataset: ', scores[1])


def update_point(trace, points, selector):
    # for point in points:
    # exclude centroids...
    if len(points.point_inds) != 0 and points.trace_name != 'Centroiden':
        img = cv2.imread(test_files[sum(yIntervals[:points.trace_index]) + points.point_inds[0]])
        img_str = cv2.imencode('.png', img)[1].tostring()
        with out_textlines:
            print(test_files[sum(yIntervals[:points.trace_index]) + points.point_inds[0]])
            # clear_output()
            display(widgets.Image(value=img_str, format='png', width=900, height=600))


def show_distribution(which_model='checkpoint.hdf5'):
    allmedians = list()
    y = list()
    save_file_path = p.Path() / '..' / 'data' / 'scribe_detector_checkpoints' / 'allmedians_learned_midTerm.npy'

    model.load_weights('..\data\scribe_detector_checkpoints\\' + which_model)

    # iterate over all images
    for i in range(len(test_files)):
        img_crops = genImgCrops(test_files[i], crop_size=model.input_shape[1], relative_num_of_crops=0.1)
        fileParts = test_files[i].split(os.path.sep)
        label = fileParts[-2]
        y.append(label)

        # get layer to retrieve features from model
        layer_model_dense2 = Model(inputs=model.input,
                                   outputs=model.get_layer('dense2').output)
        # predict features
        output_dense2 = layer_model_dense2.predict(img_crops)

        # using median to condense all feature crops to single array per image
        mean = np.mean(output_dense2, axis=0)
        median = np.median(output_dense2, axis=0)
        allmedians.append(median)
    np.save(str(save_file_path), allmedians)

    # create tsne plot
    # as tsne is a stochastic algorithm, results will not always look the same, but in average they do.
    allmedians = np.stack(allmedians)
    y = np.array(y)
    yCounter = Counter(y)
    yDict = dict(yCounter)
    global yIntervals
    yIntervals = list(yDict.values())
    target_names = list(yDict.keys())

    X_tsne = TSNE(learning_rate=50, n_components=3).fit_transform(allmedians)
    splittedData = splitData(X_tsne, yIntervals)
    centroids = centeroidnp(X_tsne, yIntervals)

    # if num_classes > 2 and < 13 you can use colorScale, else remove color setting from scatter plots
    colorScale = cl.scales[str(num_classes)]['qual']['Paired']
    data = []
    # add tsne traces
    for idx, value in enumerate(splittedData):
        trace = go.Scatter3d(x=value[0], y=value[1], z=value[2], mode='markers', name=str(target_names[idx]),
                             marker=dict(size=10, color=colorScale[idx], line=dict(color=colorScale[idx], width=0.5),
                                         opacity=0.8))
        data.append(trace)
    # add centroids
    centroidTrace = go.Scatter3d(x=np.ndarray.flatten(centroids[:, 0:1]), y=np.ndarray.flatten(centroids[:, 1:2]),
                                 z=np.ndarray.flatten(centroids[:, 2:3]),
                                 mode='markers', name="Centroiden",
                                 marker=dict(color='rgb(255, 255, 255)', size=20, symbol='circle',
                                             line=dict(color='rgb(204, 204, 204)', width=1), opacity=0.5))
    data.append(centroidTrace)

    layout = go.Layout(margin=dict(l=0, r=0, b=0, t=0), legend=dict(x=-0.1, y=1), xaxis=dict(title='nope'))
    fig = go.FigureWidget(data=data, layout=layout)
    fig.update_layout(scene=dict(xaxis=dict(title='X'), yaxis=dict(title='Y'), zaxis=dict(title='Z')))

    # add functionality to plot
    for trace in fig.data:
        trace.on_click(update_point)

    display(fig)


# widget for displaying text lines
out_textlines = widgets.Output()
# create the following structure
# put all text lines that you assume belong to one other in one folder (01,02,03,04..99)
path_to_files = p.Path() / '..' / 'data' / 'scribe_detector_data' 
# Create array of file names and corresponding target scribe names
tmp = []
target_list = []
for file in path_to_files.glob('*/*.tif'):
    tmp.append(str(file))
    target_list.append(file.parts[-2])

# transform to numpy array
img_files = np.asarray(tmp)
img_targets = np.asarray(target_list)

# encoded targets
encoder = LabelEncoder()
encoder.fit(img_targets)
encoded_Y = encoder.transform(img_targets)
# set network parameters
num_classes = len(set(target_list))#10
batch_size = 16
cropSize = 80#105
row, col, ch = cropSize, cropSize, 1
nb_epoch = 8#10
samples_per_epoch = 640#400#3268
nb_val_samples = 160#842 # 250

# Build a neural network in Keras
# BatchNormalization can be used, but does not effect this network much
model = Sequential()
model.add(ZeroPadding2D((1, 1), input_shape=(row, col, ch)))
# Resize data within the neural network
model.add(Lambda(resize_image))  #resize images to allow for easy computation
# CNN model - Building the model suggested in paper
model.add(Convolution2D(filters=32, kernel_size=(5, 5), strides=(2, 2), padding='same', name='conv1')) #96
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2), name='pool1'))
model.add(Convolution2D(filters=64, kernel_size =(3,3), strides=(1,1), padding='same', name='conv2'))  #256
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2), name='pool2'))
model.add(Convolution2D(filters=128, kernel_size=(3, 3), strides=(1, 1), padding='same', name='conv3'))  #256
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2), name='pool3'))
model.add(Flatten())
model.add(Dropout(0.5))
model.add(Dense(512, name='dense1'))
model.add(Activation('relu'))
model.add(BatchNormalization())
model.add(Dropout(0.5))
model.add(Dense(256, name='dense2'))
model.add(Activation('relu'))
model.add(BatchNormalization())
model.add(Dropout(0.5))
model.add(Dense(num_classes, name='output'+str(num_classes)))
model.add(Activation('softmax'))  #softmax since output is within 50 classes
model.compile(loss='categorical_crossentropy', optimizer=Adam(), metrics=['accuracy'])

# create a balanced data set
labels = np_utils.to_categorical(encoded_Y, num_classes)
classTotals = labels.sum(axis=0)
# scaled totals, which can be used to fight class imbalance. Ex: [1, 2.56] implies that our network will treat every
# instance of class A as 2.56 instances of class B
classWeight = classTotals.max() / classTotals
class_weight = class_weight.compute_class_weight('balanced', np.unique(encoded_Y), encoded_Y)
# split data for training process
train_files, rem_files, train_targets, rem_targets = train_test_split(img_files, encoded_Y, train_size=0.66, random_state=52)
validation_files, test_files, validation_targets, test_targets = train_test_split(rem_files, rem_targets, train_size=0.5, random_state=22)
# Generate data for training and validation
train_generator = generate_data(train_files, train_targets, cropSize=cropSize,batch_size=batch_size, factor=0.1)
validation_generator = generate_data(validation_files, validation_targets, cropSize=cropSize, batch_size=batch_size, factor=0.1)
test_generator = generate_data(test_files, test_targets, cropSize=cropSize, batch_size=batch_size, factor=0.1)
# checkpoint paths
check_location = '../data/scribe_detector_checkpoints'
filepath= os.path.join(check_location, 'checkpoint.hdf5')
# set callbacks
checkpoint = ModelCheckpoint(filepath=filepath, verbose=0, save_best_only=True)
earlystop = EarlyStopping(monitor='val_acc', min_delta=0.0001, patience=3, verbose=0, mode='auto')
callbacks_list = [checkpoint, earlystop]

# Train the model
history_object = model.fit_generator(generator=train_generator, validation_data=validation_generator,
                                     steps_per_epoch=samples_per_epoch, validation_steps=nb_val_samples,
                                     epochs=nb_epoch, verbose=1, callbacks=callbacks_list, class_weight=classWeight) 

# save history
with open(os.path.join(check_location, 'trainHistoryDict.pic'), 'wb') as file_pi:
    pickle.dump(history_object.history, file_pi)
# serialize model to JSON
model_json = model.to_json()
with open(os.path.join(check_location, 'model.json'), 'w') as json_file:
    json_file.write(model_json)

# resort test files for tsne visualisation
# possibly wrong labels because unsorted
test_targets = test_targets.reshape((test_targets.size, 1))
test_files = test_files.reshape((test_files.size, 1))
a = np.hstack((test_files, test_targets))
a = a[a[:, 1].argsort()]
test_files = [a[i][0] for i in range(a.shape[0])]
test_targets = [a[i][1] for i in range(a.shape[0])]
yIntervals = list()
